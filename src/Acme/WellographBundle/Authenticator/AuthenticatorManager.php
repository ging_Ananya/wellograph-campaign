<?php 

namespace Acme\WellographBundle\Authenticator;

use Acme\WellographBundle\Entity\UserRepository;
use Acme\WellographBundle\Entity\UserAuthRepository;
use Acme\WellographBundle\Validator\ManageValidator;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AuthenticatorManager
{
    private $manageValidator;
    private $userRepository;
    private $userAuthRepository;

    public function __construct(ManageValidator $manageValidator, UserRepository $userRepository, UserAuthRepository $userAuthRepository) 
    {
        $this->manageValidator = $manageValidator;
        $this->userRepository = $userRepository;
        $this->userAuthRepository = $userAuthRepository;
    }

	public function getUserAndLoginSuccess($username, $token)
	{

		$user = $this->userRepository->findOneByUsername($username);
		if(!$user)
        {
            throw new AuthenticationException($this->manageValidator->getCode('username_not_found'));
        }

        // $auth = $this->userAuthRepository->findOneByUsernameJoinedToUser($username);
        // if(!$auth)
        // {
        // 	throw new AuthenticationException($this->manageValidator->getCode('user_never_login'));
        // }
        // if (strcmp($token, $auth->getToken()) != 0)
        // {
        // 	throw new AuthenticationException($this->manageValidator->getCode('token_not_updated'));
        // }

        $token=trim($token); 

        $auth = $this->userAuthRepository->findOneByUsernameTokenJoinedUser($username,$token);
        if(!$auth)
        {
            throw new AuthenticationException($this->manageValidator->getCode('token_not_updated'));
        }
        
        return $user;
	}

    public function getUserAndPasswordSuccess($username, $password)
    {
        $user = $this->userRepository->findOneByUsernamePassword($username, $password);
        if(!$user)
        {
            throw new AuthenticationException($this->manageValidator->getCode('password_not_correct'));
        }

        return $user;
    }
}

