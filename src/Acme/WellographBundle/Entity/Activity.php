<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\ActivityRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Activity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="heartrate", type="smallint", nullable=true)
     */
    private $heartrate;

    /**
     * @var integer
     *
     * @ORM\Column(name="steps", type="smallint", nullable=true)
     */
    private $steps;

    /**
     * @var integer
     *
     * @ORM\Column(name="lap", type="smallint", nullable=true)
     */
    private $lap;

    /**
     * @var float
     *
     * @ORM\Column(name="distance", type="float", nullable=true)
     */
    private $distance;

    /**
     * @var float
     *
     * @ORM\Column(name="calories", type="float", nullable=true)
     */
    private $calories;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="watch_created_at", type="datetime", nullable=false)
     */
    private $watchCreatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id")
     * })
     */
    private $pair;

    /**
     * @var \Acme\WellographBundle\Entity\ActivityMode
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\ActivityMode")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mode_id", referencedColumnName="id")
     * })
     */
    private $mode;

    /**
     * @var \Acme\WellographBundle\Entity\ActivityLog
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\ActivityLog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="log_id", referencedColumnName="id")
     * })
     */
    private $log;

    /**
     * Set heartrate
     *
     * @param integer $heartrate
     * @return Activity
     */
    public function setHeartrate($heartrate)
    {
        $this->heartrate = $heartrate;
    
        return $this;
    }

    /**
     * Get heartrate
     *
     * @return integer 
     */
    public function getHeartrate()
    {
        return $this->heartrate;
    }

    /**
     * Set steps
     *
     * @param integer $steps
     * @return Activity
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    
        return $this;
    }

    /**
     * Get steps
     *
     * @return integer 
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * Set lap
     *
     * @param integer $lap
     * @return Activity
     */
    public function setLap($lap)
    {
        $this->lap = $lap;
    
        return $this;
    }

    /**
     * Get lap
     *
     * @return integer 
     */
    public function getLap()
    {
        return $this->lap;
    }

    /**
     * Set distance
     *
     * @param float $distance
     * @return Activity
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    
        return $this;
    }

    /**
     * Get distance
     *
     * @return float 
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set calories
     *
     * @param float $calories
     * @return Activity
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;
    
        return $this;
    }

    /**
     * Get calories
     *
     * @return float 
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Activity
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set watchCreatedAt
     *
     * @ORM\PrePersist
     * @param \DateTime $watchCreatedAt
     * @return DailySummary
     */
    public function setWatchCreatedAt()
    {
        $this->watchCreatedAt = new \DateTime();
    }

    /**
     * Get watchCreatedAt
     *
     * @return \DateTime 
     */
    public function getWatchCreatedAt()
    {
        return $this->watchCreatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return Activity
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * Set mode
     *
     * @param \Acme\WellographBundle\Entity\ActivityMode $mode
     * @return Activity
     */
    public function setMode(\Acme\WellographBundle\Entity\ActivityMode $mode = null)
    {
        $this->mode = $mode;
    
        return $this;
    }

    /**
     * Get mode
     *
     * @return \Acme\WellographBundle\Entity\ActivityMode 
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set log
     *
     * @param \Acme\WellographBundle\Entity\ActivityLog $log
     * @return Activity
     */
    public function setLog(\Acme\WellographBundle\Entity\ActivityLog $log = null)
    {
        $this->log = $log;
    
        return $this;
    }

    /**
     * Get log
     *
     * @return \Acme\WellographBundle\Entity\ActivityLog 
     */
    public function getLog()
    {
        return $this->log;
    }
}