<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Activity24hr
 *
 * @ORM\Table(name="activity_24hr")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\Activity24hrRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Activity24hr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id")
     * })
     */
    private $pair;

    /**
     * @var string
     *
     * @ORM\Column(name="activity24_str", type="text", nullable=false, nullable=true)
     */
    private $activity24;

    /**
     * @var smallint
     *
     * @ORM\Column(name="type", type="smallint", nullable=false, nullable=false)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="watch_created_at", type="datetime", nullable=false)
     */
    private $watchCreatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activity24
     *
     * @param string $activity24
     * @return Activity24hr
     */
    public function setActivity24($activity24)
    {
        $this->activity24 = $activity24;
    
        return $this;
    }

    /**
     * Get activity24
     *
     * @return string 
     */
    public function getActivity24()
    {
        return $this->activity24;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Activity24hr
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return Activity24hr
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set watchCreatedAt
     *
     * @param \DateTime $watchCreatedAt
     * @return Activity24hr
     */
    public function setWatchCreatedAt($watchCreatedAt)
    {
        $this->watchCreatedAt = $watchCreatedAt;
    
        return $this;
    }

    /**
     * Get watchCreatedAt
     *
     * @return \DateTime 
     */
    public function getWatchCreatedAt()
    {
        return $this->watchCreatedAt;
    }

    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return Activity24hr
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return Device
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
}