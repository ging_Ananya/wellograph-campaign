<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityFile
 *
 * @ORM\Table(name="activity_file")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\ActivityLogRepository")
 */
class ActivityFile
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id")
     * })
     */
    private $pair;


    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=300, nullable=false)
     */
    private $url;



    /**
     * @var integer
     *
     * 
     * @ORM\Column(name="data_count", type="bigint")
     */
    private $data_count;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;


    /**
     * @var integer
     *
     * 
     * @ORM\Column(name="indexfile", type="smallint")
     */
    private $indexfile;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return ActivityFile
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }



    /**
     * Set url
     *
     * @param string $url
     * @return ActivityFile
     */
    public function setUrl($url)
    {
        $this->url=$url;
    }


    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->$url;
    }




   /**
     * Set data_count
     *
     * @param integer $data_count
     * @return ActivityFile
     */
    public function setData_count($data_count)
    {
        $this->data_count=$data_count;
    }


    /**
     * Get data_count
     *
     * @return integer 
     */
    public function getData_count()
    {
        return $this->$data_count;
    }



   /**
     * Set indexfile
     *
     * @param integer $indexfile
     * @return ActivityFile
     */
    public function setIndexfile($indexfile)
    {
        $this->indexfile=$indexfile;
    }


    /**
     * Get indexfile
     *
     * @return integer 
     */
    public function getIndexfile()
    {
        return $this->$indexfile;
    }


   /**
     * Set date
     *
     * @param \DateTime $date
     * @return User
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * Set data_count
     *
     * @param integer $dataCount
     * @return ActivityFile
     */
    public function setDataCount($dataCount)
    {
        $this->data_count = $dataCount;
    
        return $this;
    }

    /**
     * Get data_count
     *
     * @return integer 
     */
    public function getDataCount()
    {
        return $this->data_count;
    }
}