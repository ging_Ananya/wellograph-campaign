<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppErrorReport
 *
 * @ORM\Table(name="app_error_report")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\AppErrorReportRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AppErrorReport
{
    //Type
    const UNKNOWN_ERROR = 0;
    const SERVICE_ERROR = 1;
    const WATCH_ERROR = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $pair;

    /**
     * @var \Acme\WellographBundle\Entity\MobilePairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\MobilePairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mobile_pair_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $mobilePairing;

     /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false, nullable=true)
     */
    private $description;

    /**
     * @var smallint
     *
     * @ORM\Column(name="token_type", type="smallint", nullable=false, nullable=false)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AppErrorReport
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return AppErrorReport
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return AppErrorReport
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return AppErrorReport
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * Set mobilePairing
     *
     * @param \Acme\WellographBundle\Entity\MobilePairing $mobilePairing
     * @return AppErrorReport
     */
    public function setMobilePairing(\Acme\WellographBundle\Entity\MobilePairing $mobilePairing = null)
    {
        $this->mobilePairing = $mobilePairing;
    
        return $this;
    }

    /**
     * Get mobilePairing
     *
     * @return \Acme\WellographBundle\Entity\MobilePairing 
     */
    public function getMobilePairing()
    {
        return $this->mobilePairing;
    }
}