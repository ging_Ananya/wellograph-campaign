<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DailySummary
 *
 * @ORM\Table(name="daily_summary")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\DailySummaryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DailySummary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="heartrate_resting", type="smallint", nullable=true)
     */
    private $heartrateResting;

    /**
     * @var integer
     *
     * @ORM\Column(name="heartrate_max", type="smallint", nullable=true)
     */
    private $heartrateMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="heartrate_min", type="smallint", nullable=true)
     */
    private $heartrateMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="heartrate_avg", type="smallint", nullable=true)
     */
    private $heartrateAvg;

    /**
     * @var float
     *
     * @ORM\Column(name="heartrate_rest_zone_duration", type="float", nullable=true)
     */
    private $heartrateRestZoneDuration;

    /**
     * @var float
     *
     * @ORM\Column(name="heartrate_light_zone_duration", type="float", nullable=true)
     */
    private $heartrateLightZoneDuration;

    /**
     * @var float
     *
     * @ORM\Column(name="heartrate_fatburn_zone_duration", type="float", nullable=true)
     */
    private $heartrateFatburnZoneDuration;

    /**
     * @var float
     *
     * @ORM\Column(name="heartrate_cardio_zone_duration", type="float", nullable=true)
     */
    private $heartrateCardioZoneDuration;

    /**
     * @var float
     *
     * @ORM\Column(name="heartrate_extream_zone_duration", type="float", nullable=true)
     */
    private $heartrateExtreamZoneDuration;

    /**
     * @var float
     *
     * @ORM\Column(name="heartrate_maximum_zone_duration", type="float", nullable=true)
     */
    private $heartrateMaximumZoneDuration;

    /**
     * @var float
     *
     * @ORM\Column(name="velocity_metre_per_sec_max", type="float", nullable=true)
     */
    private $velocityMetrePerSecMax;

    /**
     * @var float
     *
     * @ORM\Column(name="velocity_metre_per_sec_avg", type="float", nullable=true)
     */
    private $velocityMetrePerSecAvg;

    /**
     * @var float
     *
     * @ORM\Column(name="active_time", type="float", nullable=true)
     */
    private $activeTime;

    /**
     * @var smallint
     *
     * @ORM\Column(name="fitness_percentile", type="smallint", nullable=true)
     */
    private $fitnessPercentile;

    /**
     * @var smallint
     *
     * @ORM\Column(name="fitness_age", type="smallint", nullable=true)
     */
    private $fitnessAge;

    /**
     * @var float
     *
     * @ORM\Column(name="exercise_score", type="float", nullable=true)
     */
    private $exerciseScore;

    /**
     * @var integer
     *
     * @ORM\Column(name="steps", type="integer", nullable=true)
     */
    private $steps;

    /**
     * @var float
     *
     * @ORM\Column(name="distance", type="float", nullable=true)
     */
    private $distance;

    /**
     * @var float
     *
     * @ORM\Column(name="calories", type="float", nullable=true)
     */
    private $calories;

    /**
     * @var float
     *
     * @ORM\Column(name="idle_time", type="float", nullable=true)
     */
    private $idleTime;

    /**
     * @var smallint
     *
     * @ORM\Column(name="light_time_sec", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $lightTimeSec;

    /**
     * @var smallint
     *
     * @ORM\Column(name="moderate_time_sec", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $moderateTimeSec;

    /**
     * @var smallint
     *
     * @ORM\Column(name="vigorous_time_sec", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $vigorousTimeSec;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="watch_created_at", type="datetime", nullable=false)
     */
    private $watchCreatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id")
     * })
     */
    private $pair;



    /**
     * Set heartrateResting
     *
     * @param integer $heartrateResting
     * @return DailySummary
     */
    public function setHeartrateResting($heartrateResting)
    {
        $this->heartrateResting = $heartrateResting;
    
        return $this;
    }

    /**
     * Get heartrateResting
     *
     * @return integer 
     */
    public function getHeartrateResting()
    {
        return $this->heartrateResting;
    }

    /**
     * Set heartrateMax
     *
     * @param integer $heartrateMax
     * @return DailySummary
     */
    public function setHeartrateMax($heartrateMax)
    {
        $this->heartrateMax = $heartrateMax;
    
        return $this;
    }

    /**
     * Get heartrateMax
     *
     * @return integer 
     */
    public function getHeartrateMax()
    {
        return $this->heartrateMax;
    }

    /**
     * Set heartrateMin
     *
     * @param integer $heartrateMin
     * @return DailySummary
     */
    public function setHeartrateMin($heartrateMin)
    {
        $this->heartrateMin = $heartrateMin;
    
        return $this;
    }

    /**
     * Get heartrateMin
     *
     * @return integer 
     */
    public function getHeartrateMin()
    {
        return $this->heartrateMin;
    }

    /**
     * Set heartrateAvg
     *
     * @param integer $heartrateAvg
     * @return DailySummary
     */
    public function setHeartrateAvg($heartrateAvg)
    {
        $this->heartrateAvg = $heartrateAvg;
    
        return $this;
    }

    /**
     * Get heartrateAvg
     *
     * @return integer 
     */
    public function getHeartrateAvg()
    {
        return $this->heartrateAvg;
    }

    /**
     * Set heartrateRestZoneDuration
     *
     * @param float $heartrateRestZoneDuration
     * @return DailySummary
     */
    public function setHeartrateRestZoneDuration($heartrateRestZoneDuration)
    {
        $this->heartrateRestZoneDuration = $heartrateRestZoneDuration;
    
        return $this;
    }

    /**
     * Get heartrateRestZoneDuration
     *
     * @return float 
     */
    public function getHeartrateRestZoneDuration()
    {
        return $this->heartrateRestZoneDuration;
    }

    /**
     * Set heartrateLightZoneDuration
     *
     * @param float $heartrateLightZoneDuration
     * @return DailySummary
     */
    public function setHeartrateLightZoneDuration($heartrateLightZoneDuration)
    {
        $this->heartrateLightZoneDuration = $heartrateLightZoneDuration;
    
        return $this;
    }

    /**
     * Get heartrateLightZoneDuration
     *
     * @return float 
     */
    public function getHeartrateLightZoneDuration()
    {
        return $this->heartrateLightZoneDuration;
    }

    /**
     * Set heartrateFatburnZoneDuration
     *
     * @param float $heartrateFatburnZoneDuration
     * @return DailySummary
     */
    public function setHeartrateFatburnZoneDuration($heartrateFatburnZoneDuration)
    {
        $this->heartrateFatburnZoneDuration = $heartrateFatburnZoneDuration;
    
        return $this;
    }

    /**
     * Get heartrateFatburnZoneDuration
     *
     * @return float 
     */
    public function getHeartrateFatburnZoneDuration()
    {
        return $this->heartrateFatburnZoneDuration;
    }

    /**
     * Set heartrateCardioZoneDuration
     *
     * @param float $heartrateCardioZoneDuration
     * @return DailySummary
     */
    public function setHeartrateCardioZoneDuration($heartrateCardioZoneDuration)
    {
        $this->heartrateCardioZoneDuration = $heartrateCardioZoneDuration;
    
        return $this;
    }

    /**
     * Get heartrateCardioZoneDuration
     *
     * @return float 
     */
    public function getHeartrateCardioZoneDuration()
    {
        return $this->heartrateCardioZoneDuration;
    }

    /**
     * Set heartrateExtreamZoneDuration
     *
     * @param float $heartrateExtreamZoneDuration
     * @return DailySummary
     */
    public function setHeartrateExtreamZoneDuration($heartrateExtreamZoneDuration)
    {
        $this->heartrateExtreamZoneDuration = $heartrateExtreamZoneDuration;
    
        return $this;
    }

    /**
     * Get heartrateExtreamZoneDuration
     *
     * @return float 
     */
    public function getHeartrateExtreamZoneDuration()
    {
        return $this->heartrateExtreamZoneDuration;
    }

    /**
     * Set heartrateMaximumZoneDuration
     *
     * @param float $heartrateMaximumZoneDuration
     * @return DailySummary
     */
    public function setHeartrateMaximumZoneDuration($heartrateMaximumZoneDuration)
    {
        $this->heartrateMaximumZoneDuration = $heartrateMaximumZoneDuration;
    
        return $this;
    }

    /**
     * Get heartrateMaximumZoneDuration
     *
     * @return float 
     */
    public function getHeartrateMaximumZoneDuration()
    {
        return $this->heartrateMaximumZoneDuration;
    }

    /**
     * Set velocityMetrePerSecMax
     *
     * @param float $velocityMetrePerSecMax
     * @return DailySummary
     */
    public function setVelocityMetrePerSecMax($velocityMetrePerSecMax)
    {
        $this->velocityMetrePerSecMax = $velocityMetrePerSecMax;
    
        return $this;
    }

    /**
     * Get velocityMetrePerSecMax
     *
     * @return float 
     */
    public function getVelocityMetrePerSecMax()
    {
        return $this->velocityMetrePerSecMax;
    }

    /**
     * Set velocityMetrePerSecAvg
     *
     * @param float $velocityMetrePerSecAvg
     * @return DailySummary
     */
    public function setVelocityMetrePerSecAvg($velocityMetrePerSecAvg)
    {
        $this->velocityMetrePerSecAvg = $velocityMetrePerSecAvg;
    
        return $this;
    }

    /**
     * Get velocityMetrePerSecAvg
     *
     * @return float 
     */
    public function getVelocityMetrePerSecAvg()
    {
        return $this->velocityMetrePerSecAvg;
    }

    /**
     * Set activeTime
     *
     * @param float $activeTime
     * @return DailySummary
     */
    public function setActiveTime($activeTime)
    {
        $this->activeTime = $activeTime;
    
        return $this;
    }

    /**
     * Get activeTime
     *
     * @return float 
     */
    public function getActiveTime()
    {
        return $this->activeTime;
    }

    /**
     * Set fitnessPercentile
     *
     * @param integer $fitnessPercentile
     * @return DailySummary
     */
    public function setFitnessPercentile($fitnessPercentile)
    {
        $this->fitnessPercentile = $fitnessPercentile;
    
        return $this;
    }

    /**
     * Get fitnessPercentile
     *
     * @return integer 
     */
    public function getFitnessPercentile()
    {
        return $this->fitnessPercentile;
    }

    /**
     * Set fitnessAge
     *
     * @param integer $fitnessAge
     * @return DailySummary
     */
    public function setFitnessAge($fitnessAge)
    {
        $this->fitnessAge = $fitnessAge;
    
        return $this;
    }

    /**
     * Get fitnessAge
     *
     * @return integer 
     */
    public function getFitnessAge()
    {
        return $this->fitnessAge;
    }

    /**
     * Set exerciseScore
     *
     * @param float $exerciseScore
     * @return DailySummary
     */
    public function setExerciseScore($exerciseScore)
    {
        $this->exerciseScore = $exerciseScore;
    
        return $this;
    }

    /**
     * Get exerciseScore
     *
     * @return float 
     */
    public function getExerciseScore()
    {
        return $this->exerciseScore;
    }

    /**
     * Set steps
     *
     * @param float $dailyStep
     * @return DailySummary
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    
        return $this;
    }

    /**
     * Get steps
     *
     * @return float 
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * Set distance
     *
     * @param float $distance
     * @return DailySummary
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    
        return $this;
    }

    /**
     * Get distance
     *
     * @return float 
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set calories
     *
     * @param float $calories
     * @return DailySummary
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;
    
        return $this;
    }

    /**
     * Get calories
     *
     * @return float 
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Set idleTime
     *
     * @param float $idleTime
     * @return DailySummary
     */
    public function setIdleTime($idleTime)
    {
        $this->idleTime = $idleTime;
    
        return $this;
    }

    /**
     * Get idleTime
     *
     * @return float 
     */
    public function getIdleTime()
    {
        return $this->idleTime;
    }
 
    /**
     * Set lightTimeSec
     *
     * @param integer $lightTimeSec
     * @return DailySummary
     */
    public function setLightTimeSec($lightTimeSec)
    {
        $this->lightTimeSec = $lightTimeSec;
    
        return $this;
    }

    /**
     * Get lightTimeSec
     *
     * @return integer 
     */
    public function getLightTimeSec()
    {
        return $this->lightTimeSec;
    }

    /**
     * Set moderateTimeSec
     *
     * @param integer $moderateTimeSec
     * @return DailySummary
     */
    public function setModerateTimeSec($moderateTimeSec)
    {
        $this->moderateTimeSec = $moderateTimeSec;
    
        return $this;
    }

    /**
     * Get moderateTimeSec
     *
     * @return integer 
     */
    public function getModerateTimeSec()
    {
        return $this->moderateTimeSec;
    }

    /**
     * Set vigorousTimeSec
     *
     * @param integer $vigorousTimeSec
     * @return DailySummary
     */
    public function setVigorousTimeSec($vigorousTimeSec)
    {
        $this->vigorousTimeSec = $vigorousTimeSec;
    
        return $this;
    }

    /**
     * Get vigorousTimeSec
     *
     * @return integer 
     */
    public function getVigorousTimeSec()
    {
        return $this->vigorousTimeSec;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return DailySummary
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set watchCreatedAt
     *
     * @param \DateTime $watchCreatedAt
     * @return DailySummary
     */
    public function setWatchCreatedAt($watchCreatedAt)
    {
        $this->watchCreatedAt = $watchCreatedAt;
    
        return $this;
    }

    /**
     * Get watchCreatedAt
     *
     * @return \DateTime 
     */
    public function getWatchCreatedAt()
    {
        return $this->watchCreatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return DailySummary
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }
}