<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DailyWatchReport
 *
 * @ORM\Table(name="daily_watch_report")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\DailyWatchReportRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DailyWatchReport
{

    /**
     * @var integer
     *
     * @ORM\Column(name="normal_duration", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $normalDuration;

    /**
     * @var integer
     *
     * @ORM\Column(name="bluetooth_duration", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $bluetoothDuration;

    /**
     * @var integer
     *
     * @ORM\Column(name="normal_only_watch_duration", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $normalOnlyWatchDuration;

     /**
     * @var integer
     *
     * @ORM\Column(name="auto_sleep_duration", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $autoSleepDuration;

     /**
     * @var integer
     *
     * @ORM\Column(name="watch_only_duration", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $watchOnlyDuration;

     /**
     * @var integer
     *
     * @ORM\Column(name="charging_duration", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $chargingDuration;

    /**
     * @var integer
     *
     * @ORM\Column(name="normal_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $normalCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="bluetooth_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $bluetoothCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="normal_only_watch_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $normalOnlyWatchCount;

     /**
     * @var integer
     *
     * @ORM\Column(name="auto_sleep_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $autoSleepCount;

     /**
     * @var integer
     *
     * @ORM\Column(name="watch_only_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $watchOnlyCount;

     /**
     * @var integer
     *
     * @ORM\Column(name="charging_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $chargingCount;

     /**
     * @var integer
     *
     * @ORM\Column(name="normal_batt_used", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $normalBattUsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="bluetooth_batt_used", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $bluetoothBattUsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="normal_only_watch_batt_used", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $normalOnlyWatchBattUsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="auto_sleep_batt_used", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $autoSleepBattUsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="watch_only_batt_used", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $watchOnlyBattUsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="charging_batt_used", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $chargingBattUsed;

    /**
     * @var string
     *
     * @ORM\Column(name="normal_hour_duration_str", type="string",  nullable=true)
     */
    private $normalHourDurationStr;

    /**
     * @var string
     *
     * @ORM\Column(name="charging_hour_duration_str", type="string",  nullable=true)
     */
    private $chargingHourDurationStr;

    /**
     * @var integer
     *
     * @ORM\Column(name="totol_sync_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $totolSyncCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="sync_success_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $syncSuccessCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="hr_success_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $hrSuccessCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="button_press_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $buttonPressCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="charge_cycle", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $chargeCycle;

    /**
     * @var integer
     *
     * @ORM\Column(name="reset_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $resetCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="delete_log_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $deleteLogCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="shutdown_count", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $shutdownCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="watch_created_at", type="datetime", nullable=false)
     */
    private $watchCreatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id")
     * })
     */
    private $pair;

    /**
     * @var string
     *
     * @ORM\Column(name="firmware_version", type="string", length=30, nullable=true)
     */
    private $firmwareVersion;
    
    /**
     * Set normalDuration
     *
     * @param integer $normalDuration
     * @return DailyWatchReport
     */
    public function setNormalDuration($normalDuration)
    {
        $this->normalDuration = $normalDuration;
    
        return $this;
    }

    /**
     * Get normalDuration
     *
     * @return integer 
     */
    public function getNormalDuration()
    {
        return $this->normalDuration;
    }

    /**
     * Set bluetoothDuration
     *
     * @param integer $bluetoothDuration
     * @return DailyWatchReport
     */
    public function setBluetoothDuration($bluetoothDuration)
    {
        $this->bluetoothDuration = $bluetoothDuration;
    
        return $this;
    }

    /**
     * Get bluetoothDuration
     *
     * @return integer 
     */
    public function getBluetoothDuration()
    {
        return $this->bluetoothDuration;
    }

    /**
     * Set normalOnlyWatchDuration
     *
     * @param integer $normalOnlyWatchDuration
     * @return DailyWatchReport
     */
    public function setNormalOnlyWatchDuration($normalOnlyWatchDuration)
    {
        $this->normalOnlyWatchDuration = $normalOnlyWatchDuration;
    
        return $this;
    }

    /**
     * Get normalOnlyWatchDuration
     *
     * @return integer 
     */
    public function getNormalOnlyWatchDuration()
    {
        return $this->normalOnlyWatchDuration;
    }

    /**
     * Set autoSleepDuration
     *
     * @param integer $autoSleepDuration
     * @return DailyWatchReport
     */
    public function setAutoSleepDuration($autoSleepDuration)
    {
        $this->autoSleepDuration = $autoSleepDuration;
    
        return $this;
    }

    /**
     * Get autoSleepDuration
     *
     * @return integer 
     */
    public function getAutoSleepDuration()
    {
        return $this->autoSleepDuration;
    }

    /**
     * Set watchOnlyDuration
     *
     * @param integer $watchOnlyDuration
     * @return DailyWatchReport
     */
    public function setWatchOnlyDuration($watchOnlyDuration)
    {
        $this->watchOnlyDuration = $watchOnlyDuration;
    
        return $this;
    }

    /**
     * Get watchOnlyDuration
     *
     * @return integer 
     */
    public function getWatchOnlyDuration()
    {
        return $this->watchOnlyDuration;
    }

    /**
     * Set chargingDuration
     *
     * @param integer $chargingDuration
     * @return DailyWatchReport
     */
    public function setChargingDuration($chargingDuration)
    {
        $this->chargingDuration = $chargingDuration;
    
        return $this;
    }

    /**
     * Get chargingDuration
     *
     * @return integer 
     */
    public function getChargingDuration()
    {
        return $this->chargingDuration;
    }

    /**
     * Set normalCount
     *
     * @param integer $normalCount
     * @return DailyWatchReport
     */
    public function setNormalCount($normalCount)
    {
        $this->normalCount = $normalCount;
    
        return $this;
    }

    /**
     * Get normalCount
     *
     * @return integer 
     */
    public function getNormalCount()
    {
        return $this->normalCount;
    }

    /**
     * Set bluetoothCount
     *
     * @param integer $bluetoothCount
     * @return DailyWatchReport
     */
    public function setBluetoothCount($bluetoothCount)
    {
        $this->bluetoothCount = $bluetoothCount;
    
        return $this;
    }

    /**
     * Get bluetoothCount
     *
     * @return integer 
     */
    public function getBluetoothCount()
    {
        return $this->bluetoothCount;
    }

    /**
     * Set normalOnlyWatchCount
     *
     * @param integer $normalOnlyWatchCount
     * @return DailyWatchReport
     */
    public function setNormalOnlyWatchCount($normalOnlyWatchCount)
    {
        $this->normalOnlyWatchCount = $normalOnlyWatchCount;
    
        return $this;
    }

    /**
     * Get normalOnlyWatchCount
     *
     * @return integer 
     */
    public function getNormalOnlyWatchCount()
    {
        return $this->normalOnlyWatchCount;
    }

    /**
     * Set autoSleepCount
     *
     * @param integer $autoSleepCount
     * @return DailyWatchReport
     */
    public function setAutoSleepCount($autoSleepCount)
    {
        $this->autoSleepCount = $autoSleepCount;
    
        return $this;
    }

    /**
     * Get autoSleepCount
     *
     * @return integer 
     */
    public function getAutoSleepCount()
    {
        return $this->autoSleepCount;
    }

    /**
     * Set watchOnlyCount
     *
     * @param integer $watchOnlyCount
     * @return DailyWatchReport
     */
    public function setWatchOnlyCount($watchOnlyCount)
    {
        $this->watchOnlyCount = $watchOnlyCount;
    
        return $this;
    }

    /**
     * Get watchOnlyCount
     *
     * @return integer 
     */
    public function getWatchOnlyCount()
    {
        return $this->watchOnlyCount;
    }

    /**
     * Set chargingCount
     *
     * @param integer $chargingCount
     * @return DailyWatchReport
     */
    public function setChargingCount($chargingCount)
    {
        $this->chargingCount = $chargingCount;
    
        return $this;
    }

    /**
     * Get chargingCount
     *
     * @return integer 
     */
    public function getChargingCount()
    {
        return $this->chargingCount;
    }

    /**
     * Set normalBattUsed
     *
     * @param integer $normalBattUsed
     * @return DailyWatchReport
     */
    public function setNormalBattUsed($normalBattUsed)
    {
        $this->normalBattUsed = $normalBattUsed;
    
        return $this;
    }

    /**
     * Get normalBattUsed
     *
     * @return integer 
     */
    public function getNormalBattUsed()
    {
        return $this->normalBattUsed;
    }

    /**
     * Set bluetoothBattUsed
     *
     * @param integer $bluetoothBattUsed
     * @return DailyWatchReport
     */
    public function setBluetoothBattUsed($bluetoothBattUsed)
    {
        $this->bluetoothBattUsed = $bluetoothBattUsed;
    
        return $this;
    }

    /**
     * Get bluetoothBattUsed
     *
     * @return integer 
     */
    public function getBluetoothBattUsed()
    {
        return $this->bluetoothBattUsed;
    }

    /**
     * Set normalOnlyWatchBattUsed
     *
     * @param integer $normalOnlyWatchBattUsed
     * @return DailyWatchReport
     */
    public function setNormalOnlyWatchBattUsed($normalOnlyWatchBattUsed)
    {
        $this->normalOnlyWatchBattUsed = $normalOnlyWatchBattUsed;
    
        return $this;
    }

    /**
     * Get normalOnlyWatchBattUsed
     *
     * @return integer 
     */
    public function getNormalOnlyWatchBattUsed()
    {
        return $this->normalOnlyWatchBattUsed;
    }

    /**
     * Set autoSleepBattUsed
     *
     * @param integer $autoSleepBattUsed
     * @return DailyWatchReport
     */
    public function setAutoSleepBattUsed($autoSleepBattUsed)
    {
        $this->autoSleepBattUsed = $autoSleepBattUsed;
    
        return $this;
    }

    /**
     * Get autoSleepBattUsed
     *
     * @return integer 
     */
    public function getAutoSleepBattUsed()
    {
        return $this->autoSleepBattUsed;
    }

    /**
     * Set watchOnlyBattUsed
     *
     * @param integer $watchOnlyBattUsed
     * @return DailyWatchReport
     */
    public function setWatchOnlyBattUsed($watchOnlyBattUsed)
    {
        $this->watchOnlyBattUsed = $watchOnlyBattUsed;
    
        return $this;
    }

    /**
     * Get watchOnlyBattUsed
     *
     * @return integer 
     */
    public function getWatchOnlyBattUsed()
    {
        return $this->watchOnlyBattUsed;
    }

    /**
     * Set chargingBattUsed
     *
     * @param integer $chargingBattUsed
     * @return DailyWatchReport
     */
    public function setChargingBattUsed($chargingBattUsed)
    {
        $this->chargingBattUsed = $chargingBattUsed;
    
        return $this;
    }

    /**
     * Get chargingBattUsed
     *
     * @return integer 
     */
    public function getChargingBattUsed()
    {
        return $this->chargingBattUsed;
    }

    /**
     * Set normalHourDurationStr
     *
     * @param string $normalHourDurationStr
     * @return DailyWatchReport
     */
    public function setNormalHourDurationStr($normalHourDurationStr)
    {
        $this->normalHourDurationStr = $normalHourDurationStr;
    
        return $this;
    }

    /**
     * Get normalHourDurationStr
     *
     * @return string 
     */
    public function getNormalHourDurationStr()
    {
        return $this->normalHourDurationStr;
    }

    /**
     * Set chargingHourDurationStr
     *
     * @param string $chargingHourDurationStr
     * @return DailyWatchReport
     */
    public function setChargingHourDurationStr($chargingHourDurationStr)
    {
        $this->chargingHourDurationStr = $chargingHourDurationStr;
    
        return $this;
    }

    /**
     * Get chargingHourDurationStr
     *
     * @return string 
     */
    public function getChargingHourDurationStr()
    {
        return $this->chargingHourDurationStr;
    }

    /**
     * Set totolSyncCount
     *
     * @param integer $totolSyncCount
     * @return DailyWatchReport
     */
    public function setTotolSyncCount($totolSyncCount)
    {
        $this->totolSyncCount = $totolSyncCount;
    
        return $this;
    }

    /**
     * Get totolSyncCount
     *
     * @return integer 
     */
    public function getTotolSyncCount()
    {
        return $this->totolSyncCount;
    }

    /**
     * Set syncSuccessCount
     *
     * @param integer $syncSuccessCount
     * @return DailyWatchReport
     */
    public function setSyncSuccessCount($syncSuccessCount)
    {
        $this->syncSuccessCount = $syncSuccessCount;
    
        return $this;
    }

    /**
     * Get syncSuccessCount
     *
     * @return integer 
     */
    public function getSyncSuccessCount()
    {
        return $this->syncSuccessCount;
    }

    /**
     * Set hrSuccessCount
     *
     * @param integer $hrSuccessCount
     * @return DailyWatchReport
     */
    public function setHrSuccessCount($hrSuccessCount)
    {
        $this->hrSuccessCount = $hrSuccessCount;
    
        return $this;
    }

    /**
     * Get hrSuccessCount
     *
     * @return integer 
     */
    public function getHrSuccessCount()
    {
        return $this->hrSuccessCount;
    }

    /**
     * Set buttonPressCount
     *
     * @param integer $buttonPressCount
     * @return DailyWatchReport
     */
    public function setButtonPressCount($buttonPressCount)
    {
        $this->buttonPressCount = $buttonPressCount;
    
        return $this;
    }

    /**
     * Get buttonPressCount
     *
     * @return integer 
     */
    public function getButtonPressCount()
    {
        return $this->buttonPressCount;
    }

    /**
     * Set chargeCycle
     *
     * @param integer $chargeCycle
     * @return DailyWatchReport
     */
    public function setChargeCycle($chargeCycle)
    {
        $this->chargeCycle = $chargeCycle;
    
        return $this;
    }

    /**
     * Get chargeCycle
     *
     * @return integer 
     */
    public function getChargeCycle()
    {
        return $this->chargeCycle;
    }

    /**
     * Set resetCount
     *
     * @param integer $resetCount
     * @return DailyWatchReport
     */
    public function setResetCount($resetCount)
    {
        $this->resetCount = $resetCount;
    
        return $this;
    }

    /**
     * Get resetCount
     *
     * @return integer 
     */
    public function getResetCount()
    {
        return $this->resetCount;
    }

    /**
     * Set deleteLogCount
     *
     * @param integer $deleteLogCount
     * @return DailyWatchReport
     */
    public function setDeleteLogCount($deleteLogCount)
    {
        $this->deleteLogCount = $deleteLogCount;
    
        return $this;
    }

    /**
     * Get deleteLogCount
     *
     * @return integer 
     */
    public function getDeleteLogCount()
    {
        return $this->deleteLogCount;
    }

    /**
     * Set shutdownCount
     *
     * @param integer $shutdownCount
     * @return DailyWatchReport
     */
    public function setShutdownCount($shutdownCount)
    {
        $this->shutdownCount = $shutdownCount;
    
        return $this;
    }

    /**
     * Get shutdownCount
     *
     * @return integer 
     */
    public function getShutdownCount()
    {
        return $this->shutdownCount;
    }

    /**
     * Set watchCreatedAt
     *
     * @param \DateTime $watchCreatedAt
     * @return DailyWatchReport
     */
    public function setWatchCreatedAt($watchCreatedAt)
    {
        $this->watchCreatedAt = $watchCreatedAt;
    
        return $this;
    }

    /**
     * Get watchCreatedAt
     *
     * @return \DateTime 
     */
    public function getWatchCreatedAt()
    {
        return $this->watchCreatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return DailyWatchReport
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return DailyWatchReport
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set firmwareVersion
     *
     * @param string $firmwareVersion
     * @return DailyWatchReport
     */
    public function setFirmwareVersion($firmwareVersion)
    {
        $this->firmwareVersion = $firmwareVersion;
    
        return $this;
    }

    /**
     * Get firmwareVersion
     *
     * @return string 
     */
    public function getFirmwareVersion()
    {
        return $this->firmwareVersion;
    }
}