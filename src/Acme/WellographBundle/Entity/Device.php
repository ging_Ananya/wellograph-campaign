<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Device
 *
 * @ORM\Table(name="device")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\DeviceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Device
{
    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", length=50, nullable=false)
     */
    private $serialNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="dealer_id", type="bigint", nullable=true)
     */
    private $dealerId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="warranty_status", type="boolean", nullable=true)
     */
    private $warrantyStatus;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="warranty_start_at", type="datetime", nullable=true)
     */
    private $warrantyStartAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="shipping_at", type="datetime", nullable=true)
     */
    private $shippingAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actived_at", type="datetime", nullable=true)
     */
    private $activedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Hardware
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Hardware" , inversedBy="devices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hardware_id", referencedColumnName="id")
     * })
     */
    private $hardware;

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     * @return Device
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;
    
        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string 
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set dealerId
     *
     * @param integer $dealerId
     * @return Device
     */
    public function setDealerId($dealerId)
    {
        $this->dealerId = $dealerId;
    
        return $this;
    }

    /**
     * Get dealerId
     *
     * @return integer 
     */
    public function getDealerId()
    {
        return $this->dealerId;
    }

    /**
     * Set warrantyStatus
     *
     * @param boolean $warrantyStatus
     * @return Device
     */
    public function setWarrantyStatus($warrantyStatus)
    {
        if($warrantyStatus==="false" || $warrantyStatus=="0")
        {
            $warrantyStatus=0;
        }
        else
        {
            $warrantyStatus=1;   
        }

        $this->warrantyStatus = $warrantyStatus;
        return $this;
    }

    /**
     * Get warrantyStatus
     *
     * @return boolean 
     */
    public function getWarrantyStatus()
    {
        return $this->warrantyStatus;
    }

    /**
     * Set warrantyStartAt
     *
     * @param \DateTime $warrantyStartAt
     * @return Device
     */
    public function setWarrantyStartAt($warrantyStartAt)
    {
        $this->warrantyStartAt = $warrantyStartAt;
    
        return $this;
    }

    /**
     * Get warrantyStartAt
     *
     * @return \DateTime 
     */
    public function getWarrantyStartAt()
    {
        return $this->warrantyStartAt;
    }

    /**
     * Set shippingAt
     *
     * @param \DateTime $shippingAt
     * @return Device
     */
    public function setShippingAt($shippingAt)
    {
        $this->shippingAt = $shippingAt;
    
        return $this;
    }

    /**
     * Get shippingAt
     *
     * @return \DateTime 
     */
    public function getShippingAt()
    {
        return $this->shippingAt;
    }

    /**
     * Set activedAt
     *
     * @param \DateTime $activedAt
     * @return Device
     */
    public function setActivedAt($activedAt)
    {
        $this->activedAt = $activedAt;
    
        return $this;
    }

    /**
     * Get activedAt
     *
     * @return \DateTime 
     */
    public function getActivedAt()
    {
        return $this->activedAt;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return Device
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return Device
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hardware
     *
     * @param \Acme\WellographBundle\Entity\Hardware $hardware
     * @return Device
     */
    public function setHardware(\Acme\WellographBundle\Entity\Hardware $hardware = null)
    {
        $this->hardware = $hardware;
    
        return $this;
    }

    /**
     * Get hardware
     *
     * @return \Acme\WellographBundle\Entity\Hardware 
     */
    public function getHardware()
    {
        return $this->hardware;
    }
}