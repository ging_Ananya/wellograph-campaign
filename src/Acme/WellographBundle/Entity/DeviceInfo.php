<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeviceInfo
 *
 * @ORM\Table(name="device_info")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\DeviceInfoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DeviceInfo
{
    // automode
    const OFF = 0;
    const ON = 1;
    
    // timeFormat
    const FORMAT_12_HOURS = 0;
    const FORMAT_24_HOURS = 1;

    // timeDisplay
    const ANALOG = 0;
    const DIGITAL = 1;

    // startDayOfWeek
    const SUNDAY = 0;
    const MONDAY = 1;
    const SATURDAY = 2;

    // sessionIndicator
    const M_KM_TOP = 0;
    const M_KM_NOW = 1;
    const KM_H_TOP = 2;
    const KM_H_NOW = 3;
    const CAL = 4;
    const HR_NOW = 5;
    const HR_ZONE = 6;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", length=50, nullable=false)
     */
    private $serialNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=50, nullable=false)
     */
    private $model;

    /**
     * @var \Acme\WellographBundle\Entity\Firmware
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Firmware")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="firmware_id", referencedColumnName="id")
     * })
     */
    private $firmware;

    /**
     * @var string
     *
     * @ORM\Column(name="bootloader_version", type="string", length=30, nullable=false)
     */
    private $bootLoaderVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="automode", type="boolean", nullable=false , options={"default":1})
     */
    private $automode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="time_format", type="boolean", nullable=false , options={"default":1})
     */
    private $timeFormat;

    /**
     * @var smallint
     *
     * @ORM\Column(name="time_display", type="smallint", nullable=false , options={"default":1})
     */
    private $timeDisplay;

    /**
     * @var smallint
     *
     * @ORM\Column(name="startday_of_week", type="smallint", nullable=true)
     */
    private $startDayOfWeek;

    /**
     * @var smallint
     *
     * @ORM\Column(name="session_indicator", type="smallint", nullable=false , options={"default":1})
     */
    private $sessionIndicator;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DeviceInfo
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     * @return DeviceInfo
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;
    
        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string 
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return DeviceInfo
     */
    public function setModel($model)
    {
        $this->model = $model;
    
        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set bootLoaderVersion
     *
     * @param string $bootLoaderVersion
     * @return DeviceInfo
     */
    public function setBootLoaderVersion($bootLoaderVersion)
    {
        $this->bootLoaderVersion = $bootLoaderVersion;
    
        return $this;
    }

    /**
     * Get bootLoaderVersion
     *
     * @return string 
     */
    public function getBootLoaderVersion()
    {
        return $this->bootLoaderVersion;
    }

    /**
     * Set automode
     *
     * @param boolean $automode
     * @return DeviceInfo
     */
    public function setAutomode($automode)
    {
        if($automode==="false" || $automode=="0")
        {
            $automode=0;
        }
        else
        {
            $automode=1;   
        }

        $this->automode = $automode;
        return $this;
    }

    /**
     * Get automode
     *
     * @return boolean 
     */
    public function getAutomode()
    {
        return $this->automode;
    }

    /**
     * Set timeFormat
     *
     * @param boolean $timeFormat
     * @return DeviceInfo
     */
    public function setTimeFormat($timeFormat)
    {
        if($timeFormat==="false" || $timeFormat=="0")
        {
            $timeFormat=0;
        }
        else
        {
            $timeFormat=1;   
        }

        $this->timeFormat = $timeFormat;
        return $this;
    }

    /**
     * Get timeFormat
     *
     * @return boolean 
     */
    public function getTimeFormat()
    {
        return $this->timeFormat;
    }

    /**
     * Set timeDisplay
     *
     * @param integer $timeDisplay
     * @return DeviceInfo
     */
    public function setTimeDisplay($timeDisplay)
    {
        $this->timeDisplay = $timeDisplay;
    
        return $this;
    }

    /**
     * Get timeDisplay
     *
     * @return integer 
     */
    public function getTimeDisplay()
    {
        return $this->timeDisplay;
    }

    /**
     * Set startDayOfWeek
     *
     * @param integer $startDayOfWeek
     * @return DeviceInfo
     */
    public function setStartDayOfWeek($startDayOfWeek)
    {
        $this->startDayOfWeek = $startDayOfWeek;
    
        return $this;
    }

    /**
     * Get startDayOfWeek
     *
     * @return integer 
     */
    public function getStartDayOfWeek()
    {
        return $this->startDayOfWeek;
    }

    /**
     * Set sessionIndicator
     *
     * @param integer $sessionIndicator
     * @return DeviceInfo
     */
    public function setSessionIndicator($sessionIndicator)
    {
        $this->sessionIndicator = $sessionIndicator;
    
        return $this;
    }

    /**
     * Get sessionIndicator
     *
     * @return integer 
     */
    public function getSessionIndicator()
    {
        return $this->sessionIndicator;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return DeviceInfo
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return DeviceInfo
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set firmware
     *
     * @param \Acme\WellographBundle\Entity\Firmware $firmware
     * @return DeviceInfo
     */
    public function setFirmware(\Acme\WellographBundle\Entity\Firmware $firmware = null)
    {
        $this->firmware = $firmware;
    
        return $this;
    }

    /**
     * Get firmware
     *
     * @return \Acme\WellographBundle\Entity\Firmware 
     */
    public function getFirmware()
    {
        return $this->firmware;
    }
}