<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FacebookUser
 *
 * @ORM\Table(name="fb_user")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\FacebookUserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FacebookUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\User" , inversedBy="fbusers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="primary_email", type="string", length=320, nullable=false)
     */
    private $primaryEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=200, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=200, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=400, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_id", type="string", length=20, nullable=true)
     */
    private $fbId;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=70, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=10, nullable=true)
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_update_time", type="string", length=25, nullable=true)
     */
    private $fbUpdateTime;

    /**
     * @var smallint
     *
     * @ORM\Column(name="timezone", type="smallint", nullable=true)
     */
    private $timezone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="verified", type="boolean", nullable=false, options={"default":0})
     */
    private $verified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return FacebookUser
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return FacebookUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FacebookUser
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return FacebookUser
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set fbId
     *
     * @param string $fbId
     * @return FacebookUser
     */
    public function setFbId($fbId)
    {
        $this->fbId = $fbId;
    
        return $this;
    }

    /**
     * Get fbId
     *
     * @return string 
     */
    public function getFbId()
    {
        return $this->fbId;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return FacebookUser
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return FacebookUser
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    
        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set fbUpdateTime
     *
     * @param string $fbUpdateTime
     * @return FacebookUser
     */
    public function setFbUpdateTime($fbUpdateTime)
    {
        $this->fbUpdateTime = $fbUpdateTime;
    
        return $this;
    }

    /**
     * Get fbUpdateTime
     *
     * @return string 
     */
    public function getFbUpdateTime()
    {
        return $this->fbUpdateTime;
    }

    /**
     * Set timezone
     *
     * @param integer $timezone
     * @return FacebookUser
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    
        return $this;
    }

    /**
     * Get timezone
     *
     * @return integer 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     * @return FacebookUser
     */
    public function setVerified($verified)
    {
        if($verified==="false" || $verified=="0")
        {
            $verified=0;
        }
        else
        {
            $verified=1;   
        }

        $this->verified = $verified;
        return $this;
    }

    /**
     * Get verified
     *
     * @return boolean 
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return FacebookUser
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return FacebookUser
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \Acme\WellographBundle\Entity\User $user
     * @return FacebookUser
     */
    public function setUser(\Acme\WellographBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\WellographBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set primaryEmail
     *
     * @param string $primaryEmail
     * @return FacebookUser
     */
    public function setPrimaryEmail($primaryEmail)
    {
        $this->primaryEmail = $primaryEmail;
    
        return $this;
    }

    /**
     * Get primaryEmail
     *
     * @return string 
     */
    public function getPrimaryEmail()
    {
        return $this->primaryEmail;
    }
}