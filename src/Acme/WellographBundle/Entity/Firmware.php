<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Firmware
 *
 * @ORM\Table(name="firmware")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\FirmwareRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Firmware
{

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=30, nullable=false)
     */
    private $version;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="bigint", nullable=false)
     */
    private $size;

    /**
     * @var integer
     *
     * @ORM\Column(name="crc32", type="bigint", options={"unsigned"=true}, nullable=false)
     */
    private $crc32;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=200, nullable=false)
     */
    private $url;

     /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=false)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Hardware
     *
     * @ORM\ManyToMany(targetEntity="Acme\WellographBundle\Entity\Hardware", mappedBy="firmwares")
     */
    private $harewares;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_beta", type="boolean", nullable=false, options={"default":0})
     */
    private $isBeta;

    public function __construct() {
        $this->harewares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set version
     *
     * @param string $version
     * @return Firmware
     */
    public function setVersion($version)
    {
        $this->version = $version;
    
        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return iconv('windows-1252', 'utf-8', $this->version);
        // return $this->version;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Firmware
     */
    public function setStatus($status)
    {
        if($status==="false" || $status=="0")
        {
            $status=0;
        }
        else
        {
            $status=1;   
        }

        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set size
     *
     * @param integer $status
     * @return Firmware
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getSize()
    {
        return iconv('windows-1252', 'utf-8', $this->size);
        // return $this->size;
    }
    
    /**
     * Set crc32
     *
     * @param integer $status
     * @return Firmware
     */
    public function setCrc32($crc32)
    {
        $this->crc32 = $crc32;
    
        return $this;
    }

    /**
     * Get crc32
     *
     * @return integer 
     */
    public function getCrc32()
    {
        return iconv('windows-1252', 'utf-8', $this->crc32);
        // return $this->crc32;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Firmware
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return iconv('windows-1252', 'utf-8', $this->url);
        // return $this->url;
    }

    /**
     * Set comment
     *
     * @param string $url
     * @return Firmware
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return Firmware
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set hardware
     *
     * @param \Acme\WellographBundle\Entity\Hardware $hardware
     * @return Firmware
     */
    public function setHardware(\Acme\WellographBundle\Entity\Hardware $hardware = null)
    {
        $this->hardware = $hardware;
    
        return $this;
    }

    /**
     * Get hardware
     *
     * @return \Acme\WellographBundle\Entity\Hardware 
     */
    public function getHardware()
    {
        return $this->hardware;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add harewares
     *
     * @param \Acme\WellographBundle\Entity\Hardware $harewares
     * @return Firmware
     */
    public function addHareware(\Acme\WellographBundle\Entity\Hardware $harewares)
    {
        $this->harewares[] = $harewares;
    
        return $this;
    }

    /**
     * Remove harewares
     *
     * @param \Acme\WellographBundle\Entity\Hardware $harewares
     */
    public function removeHareware(\Acme\WellographBundle\Entity\Hardware $harewares)
    {
        $this->harewares->removeElement($harewares);
    }

    /**
     * Get harewares
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHarewares()
    {
        return $this->harewares;
    }

    /**
     * Set isBeta
     *
     * @param boolean $isBeta
     * @return Firmware
     */
    public function setIsBeta($isBeta)
    {
        if($isBeta==="false" || $isBeta=="0")
        {
            $isBeta=0;
        }
        else
        {
            $isBeta=1;   
        }

        $this->isBeta = $isBeta;
        return $this;
    }

    /**
     * Get isBeta
     *
     * @return boolean 
     */
    public function getIsBeta()
    {
        return $this->isBeta;
    }
}