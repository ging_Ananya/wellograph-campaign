<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hardware
 *
 * @ORM\Table(name="hardware")
 * @ORM\Entity
 */
class Hardware
{
    /**
     * @var \Acme\WellographBundle\Entity\Firmware
     *
     * @ORM\ManyToMany(targetEntity="Acme\WellographBundle\Entity\Firmware", inversedBy="hardwares")
     * @ORM\JoinTable(name="model")
     */
    private $firmwares;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

     /**
     * @var \Acme\WellographBundle\Entity\Device
     *
     * @ORM\OneToMany(targetEntity="Acme\WellographBundle\Entity\Device" ,mappedBy="hardware")
     */
    private $devices;

    public function __construct() {
        $this->firmwares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Model
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firmware
     *
     * @param \Acme\WellographBundle\Entity\Firmware $firmware
     * @return Hardware
     */
    public function setFirmware(\Acme\WellographBundle\Entity\Firmware $firmware = null)
    {
        $this->firmware = $firmware;
    
        return $this;
    }

    /**
     * Get firmware
     *
     * @return \Acme\WellographBundle\Entity\Firmware 
     */
    public function getHardware()
    {
        return $this->firmware;
    }
 
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**

     * Add devies
     *
     * @param \Acme\WellographBundle\Entity\Device $devies
     * @return Hardware
     */
    public function addDevie(\Acme\WellographBundle\Entity\Device $devies)
    {
        $this->devies[] = $devies;
    
        return $this;
    }

    /**
     * Remove devies
     *
     * @param \Acme\WellographBundle\Entity\Device $devies
     */
    public function removeDevie(\Acme\WellographBundle\Entity\Device $devies)
    {
        $this->devies->removeElement($devies);
    }

    /**
     * Get devies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDevies()
    {
        return $this->devies;
    }

    /**
     * Add firmwares
     *
     * @param \Acme\WellographBundle\Entity\Firmware $firmwares
     * @return Hardware
     */
    public function addFirmware(\Acme\WellographBundle\Entity\Firmware $firmwares)
    {
        $this->firmwares[] = $firmwares;
    
        return $this;
    }

    /**
     * Remove firmwares
     *
     * @param \Acme\WellographBundle\Entity\Firmware $firmwares
     */
    public function removeFirmware(\Acme\WellographBundle\Entity\Firmware $firmwares)
    {
        $this->firmwares->removeElement($firmwares);
    }

    /**
     * Get firmwares
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFirmwares()
    {
        return $this->firmwares;
    }

    /**
     * Add devices
     *
     * @param \Acme\WellographBundle\Entity\Device $devices
     * @return Hardware
     */
    public function addDevice(\Acme\WellographBundle\Entity\Device $devices)
    {
        $this->devices[] = $devices;
    
        return $this;
    }

    /**
     * Remove devices
     *
     * @param \Acme\WellographBundle\Entity\Device $devices
     */
    public function removeDevice(\Acme\WellographBundle\Entity\Device $devices)
    {
        $this->devices->removeElement($devices);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDevices()
    {
        return $this->devices;
    }
}