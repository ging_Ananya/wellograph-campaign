<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mobile
 *
 * @ORM\Table(name="mobile")'
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\MobileRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Mobile
{
	/**
	 * @var integer
	 * @ORM\Column(name="id", type="bigint")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	*/
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="brand", type="string", length=50, nullable=true )
	*/
	private $brand;
	
	/**
	 * @var string
	 * 
	 * @ORM\Column(name="os", type="string", length=50, nullable=true)
	 */
	private $os;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="details", type="string", length=500, nullable=true)
	 */
	private $details;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Mobile
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set brand
     *
     * @param string $brand
     * @return Mobile
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    
        return $this;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set os
     *
     * @param string $os
     * @return Mobile
     */
    public function setOs($os)
    {
        $this->os = $os;
    
        return $this;
    }

    /**
     * Get os
     *
     * @return string 
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Mobile
     */
    public function setDetails($details)
    {
        $this->details = $details;
    
        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }
}