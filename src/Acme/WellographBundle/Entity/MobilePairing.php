<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MobilePairing
 *
 * @ORM\Table(name="mobile_pairing")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\MobilePairingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MobilePairing
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id")
     * })
     */
    private $pair;

    /**
     * @var \Acme\WellographBundle\Entity\Mobile
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Mobile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mobile_id", referencedColumnName="id")
     * })
     */
    private $mobile;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="mobile_os_version", type="string", length=100, nullable=true)
	 */
	private $mobileOsVersion;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="current_app_version", type="string", length=50, nullable=true)
	 */
	private $currentAppVersion;

	/**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mobileOsVersion
     *
     * @param string $mobileOsVersion
     * @return MobilePairing
     */
    public function setMobileOsVersion($mobileOsVersion)
    {
        $this->mobileOsVersion = $mobileOsVersion;
    
        return $this;
    }

    /**
     * Get mobileOsVersion
     *
     * @return string 
     */
    public function getMobileOsVersion()
    {
        return $this->mobileOsVersion;
    }

    /**
     * Set currentAppVersion
     *
     * @param string $currentAppVersion
     * @return MobilePairing
     */
    public function setCurrentAppVersion($currentAppVersion)
    {
        $this->currentAppVersion = $currentAppVersion;
    
        return $this;
    }

    /**
     * Get currentAppVersion
     *
     * @return string 
     */
    public function getCurrentAppVersion()
    {
        return $this->currentAppVersion;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return MobilePairing
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return MobilePairing
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return MobilePairing
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * Set mobile
     *
     * @param \Acme\WellographBundle\Entity\Mobile $mobile
     * @return MobilePairing
     */
    public function setMobile(\Acme\WellographBundle\Entity\Mobile $mobile = null)
    {
        $this->mobile = $mobile;
    
        return $this;
    }

    /**
     * Get mobile
     *
     * @return \Acme\WellographBundle\Entity\Mobile 
     */
    public function getMobile()
    {
        return $this->mobile;
    }
}