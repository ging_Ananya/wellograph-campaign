<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotificationToken
 *
 * @ORM\Table(name="notification_token")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\NotificationTokenRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class NotificationToken
{
    // TokenType
    const other_os = 0;
    const ios = 1;
    const android = 2;

	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\MobilePairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\MobilePairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mobile_pair_id", referencedColumnName="id")
     * })
     */
    private $mobilePair;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="device_token", type="string", length=200, nullable=false)
	 */
	private $deviceToken;

	/**
     * @var smallint
     *
     * @ORM\Column(name="token_type", type="smallint", nullable=false)
     */
	private $tokenType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     * @return NotificationToken
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;
    
        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string 
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * Set tokenType
     *
     * @param integer $tokenType
     * @return NotificationToken
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;
    
        return $this;
    }

    /**
     * Get tokenType
     *
     * @return integer 
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return NotificationToken
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set mobilePair
     *
     * @param \Acme\WellographBundle\Entity\MobilePairing $mobilePair
     * @return NotificationToken
     */
    public function setMobilePair(\Acme\WellographBundle\Entity\MobilePairing $mobilePair = null)
    {
        $this->mobilePair = $mobilePair;
    
        return $this;
    }

    /**
     * Get mobilePair
     *
     * @return \Acme\WellographBundle\Entity\MobilePairing 
     */
    public function getMobilePair()
    {
        return $this->mobilePair;
    }
}