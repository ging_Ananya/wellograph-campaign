<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pairing
 *
 * @ORM\Table(name="pairing")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\PairingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Pairing
{
    // Status
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Acme\WellographBundle\Entity\Device
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Device")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     * })
     */
    private $device;

    /**
     * @var \Acme\WellographBundle\Entity\DeviceInfo
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\DeviceInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_info_id", referencedColumnName="id")
     * })
     */
    private $deviceInfo;

    /**
     * Set status
     *
     * @param integer $status
     * @return Pairing
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return Pairing
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return Pairing
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Acme\WellographBundle\Entity\User $user
     * @return Pairing
     */
    public function setUser(\Acme\WellographBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\WellographBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set device
     *
     * @param \Acme\WellographBundle\Entity\Device $device
     * @return Pairing
     */
    public function setDevice(\Acme\WellographBundle\Entity\Device $device = null)
    {
        $this->device = $device;
    
        return $this;
    }

    /**
     * Get device
     *
     * @return \Acme\WellographBundle\Entity\Device 
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set firmware
     *
     * @param \Acme\WellographBundle\Entity\Firmware $firmware
     * @return Pairing
     */
    public function setFirmware(\Acme\WellographBundle\Entity\Firmware $firmware = null)
    {
        $this->firmware = $firmware;
    
        return $this;
    }

    /**
     * Get firmware
     *
     * @return \Acme\WellographBundle\Entity\Firmware 
     */
    public function getFirmware()
    {
        return $this->firmware;
    }

    /**
     * Set deviceInfo
     *
     * @param \Acme\WellographBundle\Entity\DeviceInfo $deviceInfo
     * @return Pairing
     */
    public function setDeviceInfo(\Acme\WellographBundle\Entity\DeviceInfo $deviceInfo = null)
    {
        $this->deviceInfo = $deviceInfo;
    
        return $this;
    }

    /**
     * Get deviceInfo
     *
     * @return \Acme\WellographBundle\Entity\DeviceInfo 
     */
    public function getDeviceInfo()
    {
        return $this->deviceInfo;
    }
}