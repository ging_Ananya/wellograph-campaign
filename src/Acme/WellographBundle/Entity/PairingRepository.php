<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PairingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PairingRepository extends EntityRepository
{
    public function findOneByUserIdJoinedToUserAndSerialNumberJoinedToDevice($userId, $serialNumber)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT p, u, d FROM AcmeWellographBundle:Pairing p
                JOIN p.user u
                JOIN p.device d
                WHERE u.id = :userId AND d.serialNumber = :serial_number')
            ->setParameters(array('userId' => $userId, 'serial_number' => $serialNumber));

        try 
        {
            return $query->setMaxResults(1)->getSingleResult();
        } 
        catch (\Doctrine\ORM\NoResultException $e)
        {
            return null;
        }
    }

    public function findBySerialNumberJoinedToDevice($serialNumber)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT p, d FROM AcmeWellographBundle:Pairing p
                JOIN p.device d
                WHERE d.serialNumber = :serial_number')
            ->setParameter('serial_number', $serialNumber);

        try 
        {
            return $query->getResult();
        } 
        catch (\Doctrine\ORM\NoResultException $e)
        {
            return array();
        }
    }

    public function findOneWherePairIdAndUserId($pairId,$userId)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT p FROM AcmeWellographBundle:Pairing p
                WHERE p.id = :pairId AND p.user = :userId')
            ->setParameters(array('pairId' => $pairId , 'userId'=>$userId));
        try 
        {
            return $query->setMaxResults(1)->getSingleResult();
        } 
        catch (\Doctrine\ORM\NoResultException $e)
        {
            return array();
        }
    }

    public function findUserOltherDevice($user,$device)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT p FROM AcmeWellographBundle:Pairing p
                WHERE p.user = :user and p.device <> :device
                ORDER BY p.id
                ')
            ->setParameters(array('user'=> $user , 'device'=>$device));
        try 
        {
            return $query->getResult();
        } 
        catch (\Doctrine\ORM\NoResultException $e)
        {
            return array();
        }
    }
}