<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PasswordHistory
 *
 * @ORM\Table(name="password_history")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\PasswordHistoryRepository")
 * @ORM\HasLifecycleCallbacks()
*/
class PasswordHistory
{    
    /**
    * @var integer
    *
    * @ORM\Column(name="id", type="bigint")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="IDENTITY")
    */
    private $id;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="created_at", type="datetime" , nullable=false)
    */
    private $createdAt;

    /**
     * @var \Acme\WellographBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return PasswordHistory
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Acme\WellographBundle\Entity\User $user
     * @return PasswordHistory
     */
    public function setUser(\Acme\WellographBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\WellographBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}