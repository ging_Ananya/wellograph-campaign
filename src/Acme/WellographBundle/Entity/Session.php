<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\SessionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Session
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Pairing
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Pairing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pair_id", referencedColumnName="id")
     * })
     */
    private $pair;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="bigint", nullable=true)
     */
    private $duration;

    /**
     * @var float
     *
     * @ORM\Column(name="distance", type="float", nullable=true)
     */
    private $distance;

    /**
     * @var integer
     *
     * @ORM\Column(name="calories", type="bigint", nullable=true)
     */
    private $calories;

    /**
     * @var float
     *
     * @ORM\Column(name="avg_pace", type="float", nullable=true)
     */
    private $avg_pace;

    /**
     * @var float
     *
     * @ORM\Column(name="top_pace", type="float", nullable=true)
     */
    private $top_pace;

    /**
     * @var integer
     *
     * @ORM\Column(name="steps", type="bigint", nullable=true, options={"default":null} )
     */
    private $steps;

    /**
     * @var integer
     *
     * @ORM\Column(name="cardio_time", type="bigint", nullable=true, options={"default":null} )
     */
    private $cardioTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_count", type="bigint", nullable=true)
     */
    private $activityCount;

    /**
     * @var string
     *
     * @ORM\Column(name="speed_str", type="text", nullable=true)
     */
    private $speedStr;

    /**
     * @var integer
     *
     * @ORM\Column(name="light_count", type="bigint", nullable=true, options={"default":null} )
     */
    private $lightCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="moderate_count", type="bigint", nullable=true, options={"default":null} )
     */
    private $moderateCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="vigorous_count", type="bigint", nullable=true, options={"default":null} )
     */
    private $vigorousCount;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_activity_mode_str", type="text" , nullable=true)
     */
    private $subActivityModeStr;


    /**
     * @var string
     *
     * @ORM\Column(name="activity_mode_str", type="text" , nullable=true)
     */
    private $activityModeStr;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_lap", type="boolean", nullable=true)
     */
    private $isLap;

    /**
     * @var string
     *
     * @ORM\Column(name="lap_duration_str", type="text", nullable=true)
     */
    private $lapDurationStr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true , options={"default":null} )
     */
    private $deletedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true , options={"default":false} )
     */
    private $isDeleted;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     * @return Session
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    
        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime 
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     * @return Session
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    
        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Session
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set distance
     *
     * @param float $distance
     * @return Session
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    
        return $this;
    }

    /**
     * Get distance
     *
     * @return float 
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set calories
     *
     * @param integer $calories
     * @return Session
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;
    
        return $this;
    }

    /**
     * Get calories
     *
     * @return integer 
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Set avg_pace
     *
     * @param float $avgPace
     * @return Session
     */
    public function setAvgPace($avgPace)
    {
        $this->avg_pace = $avgPace;
    
        return $this;
    }

    /**
     * Get avg_pace
     *
     * @return float 
     */
    public function getAvgPace()
    {
        return $this->avg_pace;
    }

    /**
     * Set top_pace
     *
     * @param float $topPace
     * @return Session
     */
    public function setTopPace($topPace)
    {
        $this->top_pace = $topPace;
    
        return $this;
    }

    /**
     * Get top_pace
     *
     * @return float 
     */
    public function getTopPace()
    {
        return $this->top_pace;
    }

    /**
     * Set steps
     *
     * @param integer $steps
     * @return Session
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    
        return $this;
    }

    /**
     * Get steps
     *
     * @return integer 
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * Set cardioTime
     *
     * @param integer $cardioTime
     * @return Session
     */
    public function setCardioTime($cardioTime)
    {
        $this->cardioTime = $cardioTime;
    
        return $this;
    }

    /**
     * Get cardioTime
     *
     * @return integer 
     */
    public function getCardioTime()
    {
        return $this->cardioTime;
    }

    /**
     * Set activityCount
     *
     * @param integer $activityCount
     * @return Session
     */
    public function setActivityCount($activityCount)
    {
        $this->activityCount = $activityCount;
    
        return $this;
    }

    /**
     * Get activityCount
     *
     * @return integer 
     */
    public function getActivityCount()
    {
        return $this->activityCount;
    }

    /**
     * Set speedStr
     *
     * @param string $speedStr
     * @return Session
     */
    public function setSpeedStr($speedStr)
    {
        $this->speedStr = $speedStr;
    
        return $this;
    }

    /**
     * Get speedStr
     *
     * @return string 
     */
    public function getSpeedStr()
    {
        return $this->speedStr;
    }

    /**
     * Set lightCount
     *
     * @param integer $lightCount
     * @return Session
     */
    public function setLightCount($lightCount)
    {
        $this->lightCount = $lightCount;
    
        return $this;
    }

    /**
     * Get lightCount
     *
     * @return integer 
     */
    public function getLightCount()
    {
        return $this->lightCount;
    }

    /**
     * Set moderateCount
     *
     * @param integer $moderateCount
     * @return Session
     */
    public function setModerateCount($moderateCount)
    {
        $this->moderateCount = $moderateCount;
    
        return $this;
    }

    /**
     * Get moderateCount
     *
     * @return integer 
     */
    public function getModerateCount()
    {
        return $this->moderateCount;
    }

    /**
     * Set vigorousCount
     *
     * @param integer $vigorousCount
     * @return Session
     */
    public function setVigorousCount($vigorousCount)
    {
        $this->vigorousCount = $vigorousCount;
    
        return $this;
    }

    /**
     * Get vigorousCount
     *
     * @return integer 
     */
    public function getVigorousCount()
    {
        return $this->vigorousCount;
    }

    /**
     * Set activityModeStr
     *
     * @param string $activityModeStr
     * @return Session
     */
    public function setActivityModeStr($activityModeStr)
    {
        $this->activityModeStr = $activityModeStr;
    
        return $this;
    }

    /**
     * Get activityModeStr
     *
     * @return string 
     */
    public function getActivityModeStr()
    {
        return $this->activityModeStr;
    }

    /**
     * Set isLap
     *
     * @param boolean $isLap
     * @return Session
     */
    public function setIsLap($isLap)
    {
        if($isLap==="false" || $isLap=="0")
        {
            $isLap=0;
        }
        else
        {
            $isLap=1;   
        }

        $this->isLap = $isLap;
        return $this;
    }

    /**
     * Get isLap
     *
     * @return boolean 
     */
    public function getIsLap()
    {
        return $this->isLap;
    }

    /**
     * Set lapDurationStr
     *
     * @param string $lapDurationStr
     * @return Session
     */
    public function setLapDurationStr($lapDurationStr)
    {
        $this->lapDurationStr = $lapDurationStr;
    
        return $this;
    }

    /**
     * Get lapDurationStr
     *
     * @return string 
     */
    public function getLapDurationStr()
    {
        return $this->lapDurationStr;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return Session
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * Set pair
     *
     * @param \Acme\WellographBundle\Entity\Pairing $pair
     * @return Session
     */
    public function setPair(\Acme\WellographBundle\Entity\Pairing $pair = null)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return \Acme\WellographBundle\Entity\Pairing 
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Session
     */
    public function setDeletedAt($deletedAt)
    {
        if( $deletedAt != '')
        {
            $this->deletedAt = $deletedAt;
            $target_timezone = new \DateTimeZone('UTC');
            $deletedAt ->setTimeZone($target_timezone);
            $this->deletedAt = $deletedAt;
        }else
        {
            $this->deletedAt = $deletedAt;
            return $this;
        }
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Session
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set subActivityModeStr
     *
     * @param string $subActivityModeStr
     * @return Session
     */
    public function setSubActivityModeStr($subActivityModeStr)
    {
        $this->subActivityModeStr = $subActivityModeStr;
    
        return $this;
    }

    /**
     * Get subActivityModeStr
     *
     * @return string 
     */
    public function getSubActivityModeStr()
    {
        return $this->subActivityModeStr;
    }
}