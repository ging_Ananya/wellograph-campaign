<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\UserRepository")
 * @UniqueEntity(
 *     fields={"username"},
 *     message="0157"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class User
{
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=320, nullable=false)
     * @Assert\NotBlank(message="0101")
     * @Assert\Email(message = "0101", checkMX = true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message="0102")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=1, nullable=true)
     */
    private $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var integer
     *
     * @ORM\Column(name="race_id", type="integer", nullable=true)
     */
    private $raceId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="subscribe", type="boolean", nullable=true)
     */
    private $subscribe;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unit", type="boolean", nullable=true)
     */
    private $unit;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=100, nullable=true)
     */
    private $avatar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activated", type="boolean", nullable=false, options={"default":0})
     */
    private $activated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\Country
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $country;

    /**
     * @var \Acme\WellographBundle\Entity\Nationality
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\Nationality")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nation_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $nationality;

    /**
     * @var \Acme\WellographBundle\Entity\UserAuth
     *
     * @ORM\OneToMany(targetEntity="Acme\WellographBundle\Entity\UserAuth", mappedBy="user")
     */
    private $userauths;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_developer", type="boolean", nullable=false, options={"default":0})
     */
    private $isDeveloper;

    /**
     * @var smallint
     *
     * @ORM\Column(name="timezone", type="integer", nullable=true)
     */
    private $timezone;

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstname
     *
     * @param string $name
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $name
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set country
     *
     * @param \Acme\WellographBundle\Entity\Country $country
     * @return User
     */
    public function setCountry(\Acme\WellographBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \Acme\WellographBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set raceId
     *
     * @param integer $raceId
     * @return User
     */
    public function setRaceId($raceId)
    {
        $this->raceId = $raceId;
    
        return $this;
    }

    /**
     * Get raceId
     *
     * @return integer 
     */
    public function getRaceId()
    {
        return $this->raceId;
    }

    /**
     * Set nationality
     *
     * @param \Acme\WellographBundle\Entity\Nationality $nationality
     * @return User
     */
    public function setNationality(\Acme\WellographBundle\Entity\Nationality $nationality)
    {
        $this->nationality = $nationality;
    
        return $this;
    }

    /**
     * Get nationality
     *
     * @return \Acme\WellographBundle\Entity\Nationality 
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set subscribe
     *
     * @param boolean $subscribe
     * @return User
     */
    public function setSubscribe($subscribe)
    {
        if($subscribe==="false" || $subscribe=="0")
        {
            $subscribe=0;
        }
        else
        {
            $subscribe=1;   
        }

        $this->subscribe = $subscribe;    
        return $this;
    }

    /**
     * Get subscribe
     *
     * @return boolean 
     */
    public function getSubscribe()
    {
        return $this->subscribe;
    }

    /**
     * Set unit
     *
     * @param boolean $unit
     * @return User
     */
    public function setUnit($unit)
    {
        if($unit==="false" || $unit=="0")
        {
            $unit=0;
        }
        else
        {
            $unit=1;   
        }

        $this->unit = $unit;
        return $this;
    }

    /**
     * Get unit
     *
     * @return boolean 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    
        return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set activated
     *
     * @param boolean $activated
     * @return User
     */
    public function setActivated($activated)
    {
        if($activated==="false" || $activated=="0")
        {
            $activated=0;
        }
        else
        {
            $activated=1;   
        }

        $this->activated = $activated;
        return $this;
    }

    /**
     * Get activated
     *
     * @return boolean 
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt()
    {
        $updatedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $updatedAt->setTimeZone($target_timezone);
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userauths = new \Doctrine\Common\Collections\ArrayCollection();
    }
   
    /**
     * Add userauths
     *
     * @param \Acme\WellographBundle\Entity\UserAuth $userauths
     * @return User
     */
    public function addUserauth(\Acme\WellographBundle\Entity\UserAuth $userauths)
    {
        $this->userauths[] = $userauths;
    
        return $this;
    }

    /**
     * Remove userauths
     *
     * @param \Acme\WellographBundle\Entity\UserAuth $userauths
     */
    public function removeUserauth(\Acme\WellographBundle\Entity\UserAuth $userauths)
    {
        $this->userauths->removeElement($userauths);
    }

    /**
     * Get userauths
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserauths()
    {
        return $this->userauths;
    }

    /**
     * Set isDeveloper
     *
     * @param boolean $isDeveloper
     * @return User
     */
    public function setIsDeveloper($isDeveloper)
    {
        if($isDeveloper==="false" || $isDeveloper=="0")
        {
            $isDeveloper=0;
        }
        else
        {
            $isDeveloper=1;   
        }

        $this->isDeveloper = $isDeveloper;
        return $this;
    }

    /**
     * Get isDeveloper
     *
     * @return boolean 
     */
    public function getIsDeveloper()
    {
        return $this->isDeveloper;
    }

    /**
     * Set timezone
     *
     * @param integer $timezone
     * @return User
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    
        return $this;
    }

    /**
     * Get timezone
     *
     * @return integer 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }
}