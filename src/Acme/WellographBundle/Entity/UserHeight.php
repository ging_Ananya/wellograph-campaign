<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserHeight
 *
 * @ORM\Table(name="user_height")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\UserHeightRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserHeight
{
    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float", nullable=false)
     * @Assert\NotBlank(message="0107")
     */
    private $height;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Set height
     *
     * @param float $height
     * @return UserHeight
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return float 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return UserHeight
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Acme\WellographBundle\Entity\User $user
     * @return UserHeight
     */
    public function setUser(\Acme\WellographBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\WellographBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}