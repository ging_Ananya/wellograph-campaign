<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserWeight
 *
 * @ORM\Table(name="user_weight")
 * @ORM\Entity(repositoryClass="Acme\WellographBundle\Entity\UserWeightRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserWeight
{
    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", nullable=false)
     * @Assert\NotBlank(message="0108")
     */
    private $weight;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\WellographBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Acme\WellographBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Set weight
     *
     * @param float $weight
     * @return UserWeight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return UserWeight
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('UTC');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Acme\WellographBundle\Entity\User $user
     * @return UserWeight
     */
    public function setUser(\Acme\WellographBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\WellographBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}