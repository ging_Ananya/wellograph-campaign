<?php

namespace Acme\WellographBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * UserWeightRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserWeightRepository extends EntityRepository
{
	public function findOneByUserIdJoinedToUserAndOrderByCreateAt($userId)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT w, u FROM AcmeWellographBundle:UserWeight w
                JOIN w.user u
                WHERE u.id = :userId 
                ORDER BY w.createdAt DESC')
            ->setParameter('userId', $userId);

        try 
        {
            return $query->setMaxResults(1)->getSingleResult();
        } 
        catch (\Doctrine\ORM\NoResultException $e)
        {
            return null;
        }
    }

    public function findUserAndWeight($user)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT w FROM AcmeWellographBundle:UserWeight w
                WHERE w.user = :user 
                ORDER BY w.createdAt DESC')
            ->setParameter('user', $user);
        try 
        {
            return $query->setMaxResults(1)->getSingleResult();
        } 
        catch (\Doctrine\ORM\NoResultException $e)
        {
            return null;
        }
    }

}
