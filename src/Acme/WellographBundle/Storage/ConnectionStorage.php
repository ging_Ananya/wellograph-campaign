<?php

namespace Acme\WellographBundle\Storage;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
define("DEVELOP_MODE", "FALSE"); //TRUE

class ConnectionStorage
{
    public static function appErrorReportConnectionStr()
    {
        if(DEVELOP_MODE=="TRUE")
        {
            $connectionStr="DefaultEndpointsProtocol=https;AccountName=dev2activity;AccountKey=c+H1uVVRXUaThxscEXE1OiXxpMesPjixowZneO4lSRDkhkyc1QANKCtBfuzh012WOpGPoqaYfxg/KzylyaXMcw==";
        }else
        {
            $connectionStr="DefaultEndpointsProtocol=https;AccountName=apperrorreport;AccountKey=R0WwmzJ1lILwTxbSXunLEz/FXDgC7hxxc0p/xBlSZ7fKBNZr38k2wbKR7ekQVZiNePWrcaYeikrX4o7hYLNkBw==";
        }
        return $connectionStr;
    }

    public static function appErrorReportUrl()
    {
        if(DEVELOP_MODE=="TRUE")
        {
            $url="https://dev2activity.blob.core.windows.net/";
        }else
        {
            $url="https://apperrorreport.blob.core.windows.net/";
        }
        return $url;
    }


	public static function activityFileConnectionStr()
    {
    	if(DEVELOP_MODE=="TRUE")
    	{
    		$connectionStr="DefaultEndpointsProtocol=https;AccountName=dev2activity;AccountKey=c+H1uVVRXUaThxscEXE1OiXxpMesPjixowZneO4lSRDkhkyc1QANKCtBfuzh012WOpGPoqaYfxg/KzylyaXMcw==";
    	}else
    	{
    		$connectionStr="DefaultEndpointsProtocol=https;AccountName=activity;AccountKey=DtzSicq6W0IQ8MriN+IbWHH1UXz5Jwt3bcKW//Pt2U1S1xOB/nwu/QhrbWvAED2vSVjsq7fa9HuVa5piE+poKw==";
    	}
    	return $connectionStr;
    }

    public static function activityFileUrl()
    {
    	if(DEVELOP_MODE=="TRUE")
    	{
    		$url="https://dev2activity.blob.core.windows.net/";
    	}else
    	{
    		$url="https://activity.blob.core.windows.net/";
    	}
    	return $url;
    }

	public static function avatarConnectionStr()
    {
    	if(DEVELOP_MODE=="TRUE")
    	{
    		$connectionStr="DefaultEndpointsProtocol=https;AccountName=dev2activity;AccountKey=c+H1uVVRXUaThxscEXE1OiXxpMesPjixowZneO4lSRDkhkyc1QANKCtBfuzh012WOpGPoqaYfxg/KzylyaXMcw==";
    	}else
    	{
    		$connectionStr="DefaultEndpointsProtocol=https;AccountName=core;AccountKey=UHeEBxSAsqLPrCfXUfxpstz81EKxWw+ILxWdXm0kXR6RStdx05ZGhBlsIVsiGuqvqxTGI8Fos9T4humJBZo0SA==";
    	}
    	return $connectionStr;
    }

    public static function avatarUrl()
    {
    	if(DEVELOP_MODE=="TRUE")
    	{
    		$url="https://dev2activity.blob.core.windows.net/";
    	}else
    	{
    		$url="http://core.blob.core.windows.net/";
    	}
    	return $url;
    }


}