<?php

namespace Acme\WellographBundle\Validator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Aws\S3\S3Client;
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Blob\Models\CreateContainerOptions;
use WindowsAzure\Blob\Models\PublicAccessType;
use WindowsAzure\Common\ServiceException;
class ManageValidator
{
	// success code
	const SUCCESS = '0000';
	const SERVER_IS_NEWER = '0001';
	const PHONE_IS_NEWER = '0002';
	const PHONE_UP_TO_DATE = '0003';
	const FIRMWARE_UP_TO_DATE = '0004';
	const UNSUCCESS = '0005';
	const JSON_NOT_CORRECT = '0006';
	const DAILY_SUMMARY_UP_TO_DATE = '0007';
	const SESSION_UP_TO_DATE = '0008';
	const ACTIVITY24HR_UP_TO_DATE = '0009';

	// fail code
	const USERNAME_IS_REQUIRED = '0101';
	const PASSWORD_IS_REQUIRED = '0102';
	const FIRSTNAME_IS_REQUIRED = '0103';
	const LASTNAME_IS_REQUIRED = '0104';
	const GENDER_IS_REQUIRED = '0105';
	const UNIT_IS_REQUIRED = '0106';
	const BIRTHDAY_IS_REQUIRED = '0107';
	const HEIGHT_IS_REQUIRED = '0108';
	const WEIGHT_IS_REQUIRED = '0109';
	const SERIAL_NUMBER_IS_REQUIRED = '0110';
	const LAST_SYNC_ID_IS_REQUIRED = '0111';
	const LAST_UPDATED_AT_IS_REQUIRED = '0112';
	const TOKEN_IS_REQUIRED = '0113';
	const ACTIVITIES_IS_REQUIRED = '0114';
	const ACTIVITY_CREATED_AT_IS_REQUIRED = '0115';
	const DAILY_SUMMARY_CREATED_AT_IS_REQUIRED = '0116';
	const CREATED_AT_IS_REQUIRED = '0117';
	const UPDATED_AT_IS_REQUIRED = '0118';
	const LAST_ACTIVITY_AT_IS_REQUIRED = '0119';
	const STATUS_IS_REQUIRED = '0120';
	const SUBSCRIBE_IS_REQUIRED = '0121';
	const DATE_IS_REQUIRED = '0122';
	const DATA_COUNT_IS_REQUIRED = '0123';
	const DATE_MIN_IS_REQUIRED = '0124';
	const DATE_MAX_IS_REQUIRED = '0125';
	const DAILY_SUMMARIES_IS_REQUIRED = '0126';
	const REQUEST_DATE_IS_REQUIRED = '0127';
	const FROM_DATE_IS_REQUIRED = '0128';
	const TO_DATE_IS_REQUIRED = '0129';
	const ACTIVITY_FILE_IS_REQUIRED = '0130';
	const DATES_IS_REQUIRED = '0131';
	const PAIR_ID_IS_REQUIRED = '0132';
	const PROFILE_IS_REQUIRED = '0133';
	const FILE_IS_REQUIRED = '0134';
	const FIRMWARE_VERSION_IS_REQUIRED = '0135';
	const MOBILE_INFO_IS_REQUIRED = '0136';
	const DEVICE_INFO_IS_REQUIRED = '0137';
	const DAILY_WATCH_REPORT_IS_REQUIRED = '0138';
	const SESSIONS_IS_REQUIRED = '0139';
	const START_TIMES_IS_REQUIRED = '0140';
	const PRIMARY_EMAIL_IS_REQUIRED = '0141';
	const FB_USER_IS_REQUIRED = '0142';
	const START_TIME_IS_REQUIRED = '0143';
	const PASSWORD_OR_TOKEN_IS_REQUIRED = '0144';
	const ACTIVITY24HRS_IS_REQUIRED = '0145';
	const WATCH_CREATED_AT_IS_REQUIRED = '0146';
	const HRVS_IS_REQUIRED = '0147';
	const USER_ID_IS_REQUIRED = '0148';
	const MESSAGE_IS_REQUIRED = '0149';

	const USERNAME_NOT_FOUND = '0151';
	const PASSWORD_NOT_CORRECT = '0152';
	const USER_NEVER_LOGIN = '0153';
	const TOKEN_NOT_UPDATED = '0154';
	const DEVICE_HAS_NOT_BEEN_PAIRED = '0155';
	const INVALID_ACTIVITY_MODE = '0156';
	const INVALID_ACTIVITY_LOG = '0157';
	const USER_IS_ALREADY_USED = '0158';
	const OVERLAPPING_ACTIVITY_DATA = '0159';
	const OVERLAPPING_DAILY_SUMMARY_DATA = '0160';
	const DEVICE_NOT_FOUND = '0161';
	const IMAGE_NOT_FOUND = '0162';
	const USER_NOT_VERIFY_EMAIL = '0163';
	const DAILY_SUMMARY_DATA_NOT_FOUND = '0164';
	const UPLOAD_IMAGE_FAIL = '0165';
	const DATA_NOT_FOUND = '0166';
	const NOTFOUND_FIRMWARE_FOR_DEVICE = '0167';
	const DEVELOPER_NOT_FOUND = '0168';
	const CHECKSUM_NOT_CORRECT = '0169';
	const SESSION_DATA_NOT_FOUND = '0170';
	const ACTIVITY24HR_DATA_NOT_FOUND = '0171';
	const WATCH_DATE_TYPE_NOT_CORRECT = '0172';
	const DEVICE_TOKEN_NOT_FOUND = '0173';
	const USER_NOT_IN_CAMPAIGN = '0174';


	const SERVER_MAINTENANCE = '9998';
	const SERVICE_EXPIRED = '9999';

	private $errors;

	public function __construct() {
        $this->errors = array(
		    "success" => array("0000", "Success."),
		    "server_is_newer" => array("0001", "Server is newer."),
		    "ios_is_newer"=> array("0002", "Phone is newer."),
		    "ios_up_to_date" => array("0003", "Phone up to date."),
		    "firmware_up_to_date" => array("0004", "Firmware up to date"),
			"unsuccess" => array("0005", "Unsuccess."),
			"json_not_correct" => array("0006", "Json not correct."),
			"daily_summary_up_to_date" => array("0007", "Daily summary is up to date."),
			"session_up_to_date" => array("0008", "Session is up to date."),
			"activity24hr_up_to_date" => array("0009", "Activity24hr is up to date."),

		    "username_is_required"=> array("0101", "Username is required or not correct format."),
		    "password_is_required" => array("0102", "Password is required."),
		    "firstname_is_required" => array("0103", "Firstname is required."),
		    "lastname_is_required" => array("0104", "Lastname is required."),
		    "gender_is_required" => array("0105", "Gender is required."),
		    "unit_is_required" => array("0106", "Unit is required."),
		    "birthday_is_required" => array("0107", "Birthday is required or not correct format."),
		    "height_is_required" => array("0108", "Height is required."),
		    "weight_is_required" => array("0109", "Weight is required."),
		    "serial_number_is_required" => array("0110", "Serial number is required."),
		    "last_sync_id_is_required" => array("0111", "Last_sync_id is required."),
		    "last_updated_at_is_required" => array("0112", "Last_updated_at is required or not correct format."),
		    "token_is_required" => array("0113", "Token is required."),
		    "activities_is_required" => array("0114", "Activities is required."),
		    "activity_created_at_is_required" => array("0115", "Activity created-at is required or not correct format."),
		    "daily_summary_created_at_is_required" => array("0116", "Daily summary created-at is required or not correct format."),
		    "created_at_is_required" => array("0117", "Created at is required or not correct format."),
		    "updated_at_is_required" => array("0118", "Updated at is required or not correct format."),
		    "last_activity_at_is_required" => array("0119", "Last activity at is required or not correct format."),
		    "status_is_required" => array("0120", "Status is required."),
		    "subscribe_is_required" => array("0121","Subscribe is required."),
		    "date_is_required" => array("0122", "Date is required."),
			"data_count_is_required" => array("0123", "Data_count is required."),
			"date_min_is_required" => array("0124", "Date min is required."),
			"date_max_is_required" => array("0125", "Date max is required."),
			"daily_summaries_is_required" => array("0126","Daily_summaries is required"),
			"request_date_is_required" => array("0127","Request_date is required"),
			"from_date_is_required" => array("0128","From_date is required"),
			"to_date_is_required" => array("0129","To_date is required"),
			"activity_file_is_required" => array("0130","Activity_file is required"),
			"dates_is_required" => array("0131","Dates is required"),
			"pair_id_is_required" => array("0132","Pair_id is required"),
			"profile_is_required" => array("0133","Profile is required"),
			"file_is_required" => array("0134","File is required"),
			"firmware_version_is_required" => array("0135","Firmware_version is required"),
			"mobile_info_is_required" => array("0136","Mobile_info is required"),
			"device_info_is_required" => array("0137","Device_info is required"),
			"daily_watch_report_is_required" => array("0138","Daily_watch_report is required"),
			"sessions_is_required" => array("0139","Session is required"),
			"start_times_is_required" => array("0140","Start_times is required"),
			"primary_email_is_required" => array("0141","Primary_email is required"),
			"fb_user_is_required" => array("0142","Fb_user is required"),
			"start_time_is_required" => array("0143","Start_time is required"),
			"password_or_token_is_required" => array("0144","Password or token is required"),
			"activity24hrs_is_required" => array("0145","Activity24hrs is required"),
			"watch_created_at_is_required" => array("0146","Watch_created_at is required"),
			"hrvs_is_required" => array("0147","Hrvs is required"),
		    "user_id_is_required"=> array("0148", "User_id is required"),
		    "message_is_required"=> array("0149", "Message is required"),

		    "username_not_found" => array("0151", "Username not found. or username and password not correct."),
			"password_not_correct" => array("0152", "Username is found but password not correct."),
			"user_never_login" => array("0153", "Authentication fails: no record in table user_auth, user never login."),
			"token_not_updated" => array("0154", "Authentication fails: token not updated."),
			"device_has_not_been_paired" => array("0155", "Device has not been paired, pairing is null."),
			"invalid_activity_mode" => array("0156","Invalid activity mode."),
			"invalid_activity_log" => array("0157","Invalid activity log."),
			"user_is_already_used" => array("0158","Username is already used."),
			"overlapping_activity_data" => array("0159","Overlapping activity data."),
			"overlapping_daily_summary_data" => array("0160","Overlapping daily summary data."),
			"device_not_found" => array("0161", "Device not found."),
			"image_not_found" => array("0162", "Image not found."),	
			"user_not_verify_email" => array("0163", "User not verify email."),
			"daily_summary_data_not_found" => array("0164", "Daily summary data not found."),
			"upload_image_fail" => array("0165", "Upload image fail."),
			"data_not_found" => array("0166", "Data not found."),
			"not_found_firmware_for_device" => array("0167", "Not found firmware for device."),
			"developer_not_found" => array("0168", "Developer not found."),
			"checksum_not_correct" => array("0169", "Checksum not correct."),
			"session_data_not_found" => array("0170", "Session data not found."),
			"activity24hr_data_not_found" => array("0171", "Activity24hr data not found."),
			"watch_date_type_not_correct" => array("0172", "Watch_created_at type not correct."),
			"device_token_not_found" => array("0173", "Device_token not found."),
			"user_not_in_campaign" => array("174", "User don't participate in a campaign."),

			"server_maintenance" => array("9998", "Server maintenance."),
			"service_expired" => array("9999", "Service expired.")
		);
    }

    public function getCode($keyword)
    {
    	foreach ($this->errors as $key => $value) 
    	{
    		if($keyword == $key)
    		{
    			return $value[0];
    		}
    	}
    	return null;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getDescription()
    {
    	return $this->errors;
    }


 public function getDescriptionText($code)
    {
    	   foreach ($this->errors as $key => $value) 
        {
            if($code == $value[0])
            {
               return $value[1];
            }
        }
    }

 public function http_response_status($status)
    {
$http_response = array(100 => "Continue", 101 => "Switching Protocols", 102 => "Processing", 200 => "OK", 201 => "Created", 202 => "Accepted", 203 => "Non-Authoritative Information", 204 => "No Content", 205 => "Reset Content", 206 => "Partial Content", 207 => "Multi-Status", 300 => "Multiple Choices", 301 => "Moved Permanently", 302 => "Found", 303 => "See Other", 304 => "Not Modified", 305 => "Use Proxy", 306 => "(Unused)", 307 => "Temporary Redirect", 308 => "Permanent Redirect", 400 => "Bad Request", 401 => "Unauthorized", 402 => "Payment Required", 403 => "Forbidden", 404 => "Not Found", 405 => "Method Not Allowed", 406 => "Not Acceptable", 407 => "Proxy Authentication Required", 408 => "Request Timeout", 409 => "Conflict", 410 => "Gone", 411 => "Length Required", 412 => "Precondition Failed", 413 => "Request Entity Too Large", 414 => "Request-URI Too Long", 415 => "Unsupported Media Type", 416 => "Requested Range Not Satisfiable", 417 => "Expectation Failed", 418 => "I'm a teapot", 419 => "Authentication Timeout", 420 => "Enhance Your Calm", 422 => "Unprocessable Entity", 423 => "Locked", 424 => "Failed Dependency", 424 => "Method Failure", 425 => "Unordered Collection", 426 => "Upgrade Required", 428 => "Precondition Required", 429 => "Too Many Requests", 431 => "Request Header Fields Too Large", 444 => "No Response", 449 => "Retry With", 450 => "Blocked by Windows Parental Controls", 451 => "Unavailable For Legal Reasons", 494 => "Request Header Too Large", 495 => "Cert Error", 496 => "No Cert", 497 => "HTTP to HTTPS", 499 => "Client Closed Request", 500 => "Internal Server Error", 501 => "Not Implemented", 502 => "Bad Gateway", 503 => "Service Unavailable", 504 => "Gateway Timeout", 505 => "HTTP Version Not Supported", 506 => "Variant Also Negotiates", 507 => "Insufficient Storage", 508 => "Loop Detected", 509 => "Bandwidth Limit Exceeded", 510 => "Not Extended", 511 => "Network Authentication Required", 598 => "Network read timeout error", 599 => "Network connect timeout error");
	return $http_response[$status];
	}

public function testjson ($test)
{

$json=json_decode($test);
    echo "<pre>";  print_r($json); die();
}


public function writelog ($datas,$error,$function_name,$file_name,$v)
{
	
/*
				$data=json_encode($datas);

				$data="\n".date("H:i:s").":\nParmeters: ".$data."\nError: ".$error."\nFunction Name: ".$function_name."\nFile Name: ".$file_name."\nVersion: ".$v.
				"\n*******************************************************************";



			$url = "https://wellograph.s3.amazonaws.com/log_error/".date("m.d.y");
			$file_headers = @get_headers($url);
			if($file_headers[0] == 'HTTP/1.1 403 Forbidden') {
			    $data_old=null;
			}
			else {	
				$data_old = file_get_contents($url);
			}


			$data=$data_old.$data;
                $bucket = 'wellograph';
                $keyname = '/log_error';       
                $filepath = "/".date("m.d.y");
                            
                $s3= S3Client::factory(array(
                'key'    => 'AKIAINKCCE5ZX57M63HA',
                'secret' => '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',
                ));

                

       
           
            
            
             $s3->putObject(array(
                'Bucket' => $bucket,
                'Key'    => $keyname.$filepath,
                'Body'   => $data,
                'ACL'    => 'public-read'
            ));*/
           ////////////////////////////////////////////

        $data=json_encode($datas);
		$data="\n".date("H:i:s").":\nParmeters: ".$data."\nError: ".$error."\nFunction Name: ".$function_name."\nFile Name: ".$file_name."\nVersion: ".$v.
		"\n*******************************************************************";
        $containerName="errorlog";
        $fileName=date("m.d.y");
        



        $connectionString = "DefaultEndpointsProtocol=https;AccountName=core;AccountKey=UHeEBxSAsqLPrCfXUfxpstz81EKxWw+ILxWdXm0kXR6RStdx05ZGhBlsIVsiGuqvqxTGI8Fos9T4humJBZo0SA==";

        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
        $createContainerOptions = new CreateContainerOptions(); 
        $createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);
        $createContainerOptions->addMetaData("key1", "value1");
        $createContainerOptions->addMetaData("key2", "value2");
 
        $url = "https://core.blob.core.windows.net/errorlog/".date("m.d.y");
        $file_headers = @get_headers($url);

            if($file_headers[0] == 'HTTP/1.1 403 Forbidden' || 
            	$file_headers[0] == 'HTTP/1.1 404 The specified blob does not exist.' ||
            	$file_headers[0] =='HTTP/1.1 404 The specified resource does not exist.')
            {
               $data_old=null;
            }
            else {  
                    $data_old = file_get_contents($url);
                }

                
                    $data=$data_old. $data;
        try {
            $blobRestProxy->createContainer($containerName,$createContainerOptions);
        }
        catch(ServiceException $e){
        }

        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
        try {
            $blobRestProxy->createBlockBlob($containerName, $fileName,$data);
        }
        catch(ServiceException $e){
        }

           

}

 }     

