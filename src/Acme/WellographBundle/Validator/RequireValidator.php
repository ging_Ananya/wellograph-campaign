<?php

namespace Acme\WellographBundle\Validator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Acme\WellographBundle\Validator\ManageValidator;
// use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RequireValidator
{
    private $manageValidator;

    public function __construct(ManageValidator $manageValidator) {
        $this->manageValidator = $manageValidator;
    }

	public function validate(Request $request, $params)
    {
        foreach($params as $param){
            $require = $request->request->get($param);
            if(!isset($require) || trim($require)==='')
            {
                $message = $this->manageValidator->getCode($param . '_is_required');
                throw new InvalidArgumentException($message);
            }
        }
    }

    public function validateOr(Request $request, $params)
    {
        $paramStr='';
        $haveParameter = false;
        foreach($params as $param)
        {
            $require = $request->request->get($param);
            if(isset($require) || trim($require)!='')
            {
                $haveParameter = true;
                break; 
            }
            else
            {
                if (trim($paramStr)=='')
                {
                    $paramStr = $param;
                }else
                {
                    $paramStr = $paramStr.'_or_'.$param;
                }
            }
        }

        if ($haveParameter == false)
        {   
            $message = $this->manageValidator->getCode($paramStr . '_is_required');
            throw new InvalidArgumentException($message);
        }
    }

}