<?php

namespace Acme\WellographCampaignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeWellographCampaignBundle:Default:index.html.twig', array('name' => $name));
    }
}
