<?php

namespace Acme\WellographCampaignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\DomCrawler\Crawler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Acme\WellographBundle\Entity\User;
use Acme\WellographBundle\Entity\UserAuth;
use Acme\WellographBundle\Entity\Pairing;
use Acme\WellographCampaignBundle\Entity\WalkForLifeUser;
use Acme\WellographCampaignBundle\Entity\WalkForLifeSumSteps;
use Acme\WellographBundle\Validator\ManageValidator;
use \DateTime;

class WalkForLifeController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeWellographCampaignBundle:Default:index.html.twig', array('name' => $name));
    }

    /**
    * @Template("AcmeWellographCampaignBundle:WalkForLife:result.xml.twig")
    */
    public function acceptAction(Request $request)
    {
    	$mvt= new ManageValidator;
        try
        {
            $validator = $this->get('require_validator');
            $validator->validate($request, array(
                "username",
                "token",
                "pair_id"
                ));
        }
        catch (InvalidArgumentException $failed) 
        {
            return array('status' => http_response_code(),
                    'message' => $mvt->http_response_status(http_response_code()),
                    'code' =>   $failed->getMessage(), 
                    'description' => $mvt->getDescriptionText($failed->getMessage()));
        }

        $username=$request->request->get('username');
        $token=$request->request->get('token');
        $pairId=$request->request->get('pair_id');
        try
        {   
            $authenManager = $this->get('authen_manager');
            $user = $authenManager->getUserAndLoginSuccess($username, $token);
            $userId = $user->getId();
        } 
        catch (AuthenticationException $failed) 
        {    
            return array('status' => http_response_code(),
                'message' => $mvt->http_response_status(http_response_code()),
                'code' =>   $failed->getMessage(), 
                'description' => $mvt->getDescriptionText($failed->getMessage()));
        } 
        
        /* query pairing */
        $em = $this->getDoctrine()->getManager('api');
        $connection = $em->getConnection();

        $sql = "SELECT p.user_id user_id,p.id pair_id, d.id device_id, d.serial_number FROM pairing p LEFT JOIN device d ON (p.device_id=d.id)"
                ." WHERE p.id = ".$pairId
                ." LIMIT 1";
        $statement = $connection->prepare($sql);
        $statement->execute();
    
        $pairArr = $statement->fetch();

        /* check user_id & pair_id is accepted */
        $em = $this->getDoctrine()->getManager();
        $wflUsers = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
             ->findByUserIdWhereEndedIsNull($userId);
        if($wflUsers)
        {
            $now = new \DateTime("now");
            foreach ($wflUsers as $wflUser) 
            {
                $wflUser->setEndedAt($now);
            }
            $em->persist($wflUser);
            $em->flush();
        }

        $wflDevices = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
             ->findSerialNumberWhereEndedIsNull($pairArr['serial_number']);
        if($wflDevices)
        {
            $now = new \DateTime("now");
            foreach ($wflDevices as $wflDevice) 
            {
                $wflDevice->setEndedAt($now);
            }
            $em->persist($wflDevice);
            $em->flush();
        }

        /* new pair_id */
        // $em = $this->getDoctrine()->getManager();
        // $wflNewPair = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
        //      ->findOneByPairId($pairId);
        // if(!$wflNewPair)
        // {
            $wflNewPair = new WalkForLifeUser();
            $wflNewPair->setUserId($userId);     
            $wflNewPair->setPairId($pairId);
            $wflNewPair->setSerialNumber($pairArr['serial_number']);
            $em->persist($wflNewPair);
            $em->flush();
        // }

        return array('status' => http_response_code(),
            'message' => $mvt->http_response_status(http_response_code()),
            'code' =>   ManageValidator::SUCCESS, 
            'description' => $mvt->getDescriptionText(ManageValidator::SUCCESS)
            );
    }


    /**
    * @Template("AcmeWellographCampaignBundle:WalkForLife:result.xml.twig")
    */
    public function changePairAction(Request $request)
    {
        $mvt= new ManageValidator;
        try
        {
            $validator = $this->get('require_validator');
            $validator->validate($request, array(
                "username",
                "token",
                "pair_id"
                ));
        }
        catch (InvalidArgumentException $failed) 
        {
            return array('status' => http_response_code(),
                    'message' => $mvt->http_response_status(http_response_code()),
                    'code' =>   $failed->getMessage(), 
                    'description' => $mvt->getDescriptionText($failed->getMessage()));
        }

        $username=$request->request->get('username');
        $token=$request->request->get('token');
        $pairId=$request->request->get('pair_id');
        try
        {   
            $authenManager = $this->get('authen_manager');
            $user = $authenManager->getUserAndLoginSuccess($username, $token);
            $userId = $user->getId();
        } 
        catch (AuthenticationException $failed) 
        {    
            return array('status' => http_response_code(),
                'message' => $mvt->http_response_status(http_response_code()),
                'code' =>   $failed->getMessage(), 
                'description' => $mvt->getDescriptionText($failed->getMessage()));
        } 
        
        /* query pairing */
        $em = $this->getDoctrine()->getManager('api');
        $connection = $em->getConnection();

        $sql = "SELECT p.id pair_id, d.id device_id, d.serial_number FROM pairing p LEFT JOIN device d ON (p.device_id=d.id)"
                ." WHERE p.id = ".$pairId
                ." LIMIT 1";
        $statement = $connection->prepare($sql);
        $statement->execute();
        $pairArr = $statement->fetch();

        /* check user_id & pair_id is accepted */
        $em = $this->getDoctrine()->getManager();
        $wflUsers = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
             ->findByUserIdWhereEndedIsNull($userId);
        if($wflUsers)
        {
            $now = new \DateTime("now");
            foreach ($wflUsers as $wflUser) 
            {
                $wflUser->setEndedAt($now);
            }
            $em->persist($wflUser);
            $em->flush();
        }

        $wflDevices = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
             ->findSerialNumberWhereEndedIsNull($pairArr['serial_number']);
        if($wflDevices)
        {
            $now = new \DateTime("now");
            foreach ($wflDevices as $wflDevice) 
            {
                $wflDevice->setEndedAt($now);
            }
            $em->persist($wflDevice);
            $em->flush();
        }

        /* new pair_id */
        // $em = $this->getDoctrine()->getManager();
        // $wflNewPair = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
        //      ->findOneByPairId($pairId);
        // if(!$wflNewPair)
        // {
            $wflNewPair = new WalkForLifeUser();
            $wflNewPair->setUserId($userId);     
            $wflNewPair->setPairId($pairId);
            $wflNewPair->setSerialNumber($pairArr['serial_number']);
            $em->persist($wflNewPair);
            $em->flush();
        // }

        return array('status' => http_response_code(),
                'message' => $mvt->http_response_status(http_response_code()),
                'code' =>   ManageValidator::SUCCESS, 
                'description' => $mvt->getDescriptionText(ManageValidator::SUCCESS)
                );
    }

    /**
    * @Template("AcmeWellographCampaignBundle:WalkForLife:sumsteps.xml.twig")
    */
    public function sumStepsAction(Request $request)
    {
        $mvt= new ManageValidator;
        try
        {
            $validator = $this->get('require_validator');
            $validator->validate($request, array(
                "username",
                "token"
                ));
        }
        catch (InvalidArgumentException $failed) 
        {
            return array('status' => http_response_code(),
                    'message' => $mvt->http_response_status(http_response_code()),
                    'code' =>   $failed->getMessage(), 
                    'description' => $mvt->getDescriptionText($failed->getMessage()));
        }


        $username=$request->request->get('username');
        $token=$request->request->get('token');

        try
        {   
            $authenManager = $this->get('authen_manager');
            $user = $authenManager->getUserAndLoginSuccess($username, $token);
            $userId = $user->getId();
        } 
        catch (AuthenticationException $failed) 
        {    
            return array('status' => http_response_code(),
                'message' => $mvt->http_response_status(http_response_code()),
                'code' =>   $failed->getMessage(), 
                'description' => $mvt->getDescriptionText($failed->getMessage()));
        } 


        /* query pair_id by user_id */
        $em = $this->getDoctrine()->getManager();
        $wflUsers = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
             ->findUserIdOrderCreatedAt($userId);
        if( $wflUsers )
        {
            $c = count($wflUsers);
            $i = 0;
            $sqlWhere = ' WHERE';
            $sqlCase = '( SELECT CASE';
            $sqlFinish = '( SELECT CASE';
            foreach ( $wflUsers as $wflUser) 
            {
                $sqlWhere = $sqlWhere.' ( pair_id = '.$wflUser->getPairId().' and date(watch_created_at) between';
                $sqlCase = $sqlCase.' WHEN ( pair_id = '.$wflUser->getPairId().' and date(watch_created_at) between';
                $sqlFinish = $sqlFinish.' WHEN ( pair_id = '.$wflUser->getPairId().' and date(watch_created_at) between';

                if ( $wflUser->getEndedAt() )
                { 
                    $endedObj = new \DateTime($wflUser->getEndedAt()->format('Y-m-d H:i:s'),new \DateTimeZone('Asia/Bangkok') );
                    $wflEnded = $wflUser->getEndedAt()->format('Y-m-d H:i:s');
                }
                else
                {   
                    $endedObj = new \DateTime(WalkForLifeUser::finishWfl,new \DateTimeZone('Asia/Bangkok') ); 
                    $wflEnded = WalkForLifeUser::finishWfl;
                }
                $createdObj = new \DateTime($wflUser->getCreatedAt()->format('Y-m-d H:i:s'),new \DateTimeZone('Asia/Bangkok') );

                $target_timezone = new \DateTimeZone('UTC');
                $createdObj -> setTimeZone($target_timezone);
                $endedObj -> setTimeZone($target_timezone);

                $createdStr= $createdObj->format('Y-m-d H:i:s');
                $endedStr= $endedObj->format('Y-m-d H:i:s');

                $sqlWhere = $sqlWhere.' \''.$createdStr.'\' and \''.$endedStr.'\' )';
                $sqlCase = $sqlCase.' \''.$createdStr.'\' and \''.$endedStr.'\' ) ';
                $sqlCase = $sqlCase.' THEN \''.$wflUser->getCreatedAt()->format('Y-m-d H:i:s').'\'';
                $sqlFinish = $sqlFinish.' \''.$createdStr.'\' and \''.$endedStr.'\' ) ';
                $sqlFinish = $sqlFinish.' THEN \''.$wflEnded.'\'';
                
                $i = $i + 1;
                if($i<$c)
                {
                    $sqlWhere = $sqlWhere.' or';   
                }
            }
            $sqlCase = $sqlCase.' ELSE \'OTHER DATE\' END) as group_type';
            $sqlFinish = $sqlFinish.' ELSE \'OTHER DATE\' END) as end_type';
        }
        else
        {
            return $this->render('AcmeWellographCampaignBundle:WalkForLife:result.xml.twig', 
                array('status' => http_response_code(),
                    'message' => $mvt->http_response_status(http_response_code()),
                    'code' =>   ManageValidator::USER_NOT_IN_CAMPAIGN, 
                    'description' => $mvt->getDescriptionText(ManageValidator::USER_NOT_IN_CAMPAIGN)));   
        }

        $sql =  'select pair_id,serial_number,sum(step) as sum_steps'
                .' ,'.$sqlCase
                .' ,'.$sqlFinish
                .' from ( select d.id,pair_id,v.serial_number as serial_number,max(steps) as step,watch_created_at'
                .' from daily_summary d'
                .' left join pairing p on (p.id=d.pair_id)'
                .' left join device v on (v.id=p.device_id)'
                .$sqlWhere.' and p.user_id = '.$userId
                .' group by pair_id,date(watch_created_at)'
                .' ) user_steps'
                .' group by pair_id ,group_type ';

        $em = $this->getDoctrine()->getManager('api');
        $connection = $em->getConnection();

        $statement = $connection->prepare($sql);
        $statement->execute();
        $rsApi = $statement->fetchAll();

        $i = 0;
        $arrSumSteps = null ;
        $totalSteps = 0;
        $totalAmount = 0;
        foreach($rsApi as $rs)
        {
            $wflSumSteps['pair_id'] = $rs['pair_id'];
            $wflSumSteps['from_date'] = $rs['group_type'];
            if($rs['end_type'])
            {
                $endedAt = $rs['end_type'];
            }
            else
            {
                $endedAt = WalkForLifeUser::finishWfl;
            }
            $wflSumSteps['to_date'] = $endedAt ;
            $wflSumSteps['serial_number'] = $rs['serial_number'] ;
            $wflSumSteps['steps'] = $rs['sum_steps'];
            // calculator
            $amount = $this->calculateAmount( $rs['sum_steps'] );
            $wflSumSteps['amount'] = number_format($amount, 2, '.', '');
            
            $totalSteps = $totalSteps + $wflSumSteps['steps'];
            $totalAmount = $totalAmount + $wflSumSteps['amount'];

            $arrSumSteps[$i]=$wflSumSteps;
            $wflSumSteps = null;
            $i = $i+1;
        }

        $jsonSumSteps= json_encode($arrSumSteps,true);
        $totalAmount = number_format($totalAmount, 2, '.', '');

        return array('status' => http_response_code(),
            'message' => $mvt->http_response_status(http_response_code()),
            'code' =>   ManageValidator::SUCCESS, 
            'description' => $mvt->getDescriptionText(ManageValidator::SUCCESS),
            'sumSteps' => $jsonSumSteps,
            'totalSteps' => $totalSteps,
            'totalAmount' => $totalAmount );

    }


    /**
    * @Template("AcmeWellographCampaignBundle:WalkForLife:user_show.html.twig")
    */
    public function userShowAction(Request $request)
    {
        $mvt= new ManageValidator;
        try
        {
            $validator = $this->get('require_validator');
            $validator->validate($request, array(
                "username",
                "token"
                ));
        }
        catch (InvalidArgumentException $failed) 
        {
            return array('status' => http_response_code(),
                    'message' => $mvt->http_response_status(http_response_code()),
                    'code' =>   $failed->getMessage(), 
                    'description' => $mvt->getDescriptionText($failed->getMessage()));
        }

        $username=$request->request->get('username');
        $token=$request->request->get('token');

        try
        {   
            $authenManager = $this->get('authen_manager');
            $user = $authenManager->getUserAndLoginSuccess($username, $token);
            $userId = $user->getId();
        } 
        catch (AuthenticationException $failed) 
        {    
            return array('status' => http_response_code(),
                'message' => $mvt->http_response_status(http_response_code()),
                'code' =>   $failed->getMessage(), 
                'description' => $mvt->getDescriptionText($failed->getMessage()));
        } 

        /* query pair_id by user_id */
        $em = $this->getDoctrine()->getManager();
        $wflUsers = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
             ->findUserIdOrderCreatedAt($userId);
        if( $wflUsers )
        {
            $c = count($wflUsers);
            $i = 0;
            $sqlWhere = ' WHERE (';
            $sqlCase = '( SELECT CASE';
            $sqlFinish = '( SELECT CASE';
            foreach ( $wflUsers as $wflUser) 
            {
                $sqlWhere = $sqlWhere.' ( pair_id = '.$wflUser->getPairId().' and date(watch_created_at) between';
                $sqlCase = $sqlCase.' WHEN ( pair_id = '.$wflUser->getPairId().' and date(watch_created_at) between';
                $sqlFinish = $sqlFinish.' WHEN ( pair_id = '.$wflUser->getPairId().' and date(watch_created_at) between';
                
                if ( $wflUser->getEndedAt() )
                { 
                    $endedObj = new \DateTime($wflUser->getEndedAt()->format('Y-m-d H:i:s'),new \DateTimeZone('Asia/Bangkok') );
                    $wflEnded = $wflUser->getEndedAt()->format('Y-m-d H:i:s');
                }
                else
                {   
                    $endedObj = new \DateTime(WalkForLifeUser::finishWfl,new \DateTimeZone('Asia/Bangkok') ); 
                    $wflEnded = WalkForLifeUser::finishWfl;
                }
                $createdObj = new \DateTime($wflUser->getCreatedAt()->format('Y-m-d H:i:s'),new \DateTimeZone('Asia/Bangkok') );

                $target_timezone = new \DateTimeZone('UTC');
                $createdObj -> setTimeZone($target_timezone);
                $endedObj -> setTimeZone($target_timezone);

                $createdStr= $createdObj->format('Y-m-d H:i:s');
                $endedStr= $endedObj->format('Y-m-d H:i:s');

                $sqlWhere = $sqlWhere.' \''.$createdStr.'\' and \''.$endedStr.'\' )';
                $sqlCase = $sqlCase.' \''.$createdStr.'\' and \''.$endedStr.'\' ) ';
                $sqlCase = $sqlCase.' THEN \''.$wflUser->getCreatedAt()->format('Y-m-d H:i:s').'\'';
                $sqlFinish = $sqlFinish.' \''.$createdStr.'\' and \''.$endedStr.'\' ) ';
                $sqlFinish = $sqlFinish.' THEN \''.$wflEnded.'\'';
                
                $i = $i + 1;
                if($i<$c)
                {
                    $sqlWhere = $sqlWhere.' or';   
                }
            }
            $sqlCase = $sqlCase.' ELSE \'OTHER DATE\' END) as group_type';
            $sqlFinish = $sqlFinish.' ELSE \'OTHER DATE\' END) as end_type';
        }
        else
        {
            return $this->render('AcmeWellographCampaignBundle:WalkForLife:result.xml.twig', 
                array('status' => http_response_code(),
                    'message' => $mvt->http_response_status(http_response_code()),
                    'code' =>   ManageValidator::USER_NOT_IN_CAMPAIGN, 
                    'description' => $mvt->getDescriptionText(ManageValidator::USER_NOT_IN_CAMPAIGN)));   
        }

        $sql =  'select pair_id,serial_number,sum(step) as sum_steps'
                .' ,'.$sqlCase
                .' ,'.$sqlFinish
                .' from ( select d.id,pair_id,v.serial_number as serial_number,max(steps) as step,watch_created_at'
                .' from daily_summary d'
                .' left join pairing p on (p.id=d.pair_id)'
                .' left join device v on (v.id=p.device_id)'
                .$sqlWhere.') and p.user_id = '.$userId
                .' group by pair_id,date(watch_created_at)'
                .' ) user_steps'
                .' group by pair_id, group_type';

        $em = $this->getDoctrine()->getManager('api');
        $connection = $em->getConnection();

        $statement = $connection->prepare($sql);
        $statement->execute();
        $rsApi = $statement->fetchAll();

        $i = 0;
        $arrSumSteps = null ;
        $totalSteps = 0;
        $totalAmount = 0;
        foreach($rsApi as $rs)
        {
            $wflSumSteps['pair_id'] = $rs['pair_id'];
            $wflSumSteps['from_date'] = $rs['group_type'];
            if($rs['end_type'])
            {
                $endedAt = $rs['end_type'];
            }
            else
            {
                $endedAt = WalkForLifeUser::finishWfl;
            }
            $wflSumSteps['to_date'] = $endedAt ;
            $wflSumSteps['serial_number'] = $rs['serial_number'] ;
            $wflSumSteps['steps'] = $rs['sum_steps'];
            // calculator
            $amount = $this->calculateAmount( $rs['sum_steps'] );
            $wflSumSteps['amount'] = number_format($amount, 2, '.', ',');
            
            $totalSteps = $totalSteps + $wflSumSteps['steps'];
            $totalAmount = $totalAmount + $wflSumSteps['amount'];

            $arrSumSteps[$i]=$wflSumSteps;
            $wflSumSteps = null;
            $i = $i+1;
        }

        return array(   'username' => $username,
                        'user_id' => $userId,
                        'arrSumSteps' => $arrSumSteps );
    }


    public function calculateAmount($steps)
    {
        $amount = ( $steps/20000 ) *1;
        return $amount;
    }


    /**
    * @Template("AcmeWellographCampaignBundle:WalkForLife:result.xml.twig")
    */
    public function quitAction(Request $request)
    {
        $mvt= new ManageValidator;
        try
        {
            $validator = $this->get('require_validator');
            $validator->validate($request, array(
                "username",
                "token",
                "pair_id"
                ));
        }
        catch (InvalidArgumentException $failed) 
        {
            return array('status' => http_response_code(),
                    'message' => $mvt->http_response_status(http_response_code()),
                    'code' =>   $failed->getMessage(), 
                    'description' => $mvt->getDescriptionText($failed->getMessage()));
        }

        $username=$request->request->get('username');
        $token=$request->request->get('token');
        $pairId=$request->request->get('pair_id');
        try
        {   
            $authenManager = $this->get('authen_manager');
            $user = $authenManager->getUserAndLoginSuccess($username, $token);
            $userId = $user->getId();
        } 
        catch (AuthenticationException $failed) 
        {    
            return array('status' => http_response_code(),
                'message' => $mvt->http_response_status(http_response_code()),
                'code' =>   $failed->getMessage(), 
                'description' => $mvt->getDescriptionText($failed->getMessage()));
        } 

        /* check user_id & pair_id is accepted */
        $em = $this->getDoctrine()->getManager();
        $walkForLifeUsers = $em->getRepository('AcmeWellographCampaignBundle:WalkForLifeUser')
             ->findpairIdWhereEndedIsNull($pairId);
        if($walkForLifeUsers)
        {
            $now = new \DateTime("now");
            foreach ($walkForLifeUsers as $walkForLifeUser) 
            {
                $walkForLifeUser->setEndedAt($now);
            }
            $em->persist($walkForLifeUser);
            $em->flush();
        }

        return array('status' => http_response_code(),
            'message' => $mvt->http_response_status(http_response_code()),
            'code' =>   ManageValidator::SUCCESS, 
            'description' => $mvt->getDescriptionText(ManageValidator::SUCCESS)
            );
    }

}