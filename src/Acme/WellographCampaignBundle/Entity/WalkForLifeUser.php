<?php

namespace Acme\WellographCampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * WalkForLifeUser
 *
 * @ORM\Table(name="walkforlife_user")
 * @ORM\Entity(repositoryClass="Acme\WellographCampaignBundle\Entity\WalkForLifeUserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class WalkForLifeUser
{

    // end walkforlife campaign
    const finishWfl = '2015-12-31 23:59:59';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

     /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     */
    private $userId;

     /**
     * @var integer
     *
     * @ORM\Column(name="pair_id", type="bigint", nullable=false)
     */
    private $pairId;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", length=50, nullable=false)
     */
    private $serialNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ended_at", type="datetime", nullable=true)
     */
    private $endedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return WalkForLifeUser
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return WalkForLifeUser
     */
    public function setCreatedAt()
    {
        $createdAt = new \DateTime();
        $target_timezone = new \DateTimeZone('Asia/Bangkok');
        $createdAt ->setTimeZone($target_timezone);
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set pairId
     *
     * @param integer $pairId
     * @return WalkForLifeUser
     */
    public function setPairId($pairId)
    {
        $this->pairId = $pairId;
    
        return $this;
    }

    /**
     * Get pairId
     *
     * @return integer 
     */
    public function getPairId()
    {
        return $this->pairId;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     * @return WalkForLifeUser
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;
    
        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string 
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set endedAt
     *
     * @param \DateTime $endedAt
     * @return WalkForLifeUser
     */
    public function setEndedAt($endedAt)
    {
        $endedAt = new \DateTime();
        $target_timezone = new \DateTimeZone('Asia/Bangkok');
        $endedAt ->setTimeZone($target_timezone);
        $this->endedAt = $endedAt;
        return $this;
    }

    /**
     * Get endedAt
     *
     * @return \DateTime 
     */
    public function getEndedAt()
    {
        return $this->endedAt;
    }
}