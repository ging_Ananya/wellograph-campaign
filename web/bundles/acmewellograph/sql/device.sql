-- activity mode
INSERT INTO activity_mode (`name`) VALUES ('passive');
INSERT INTO activity_mode (`name`) VALUES ('active');
INSERT INTO activity_mode (`name`) VALUES ('aggressive');
INSERT INTO activity_mode (`name`) VALUES ('session');

-- firmware
INSERT INTO firmware VALUES (1, '1.0.0', 1, 2147483647, 2147483647, 'www.wellograph.com', '2014-06-02');

-- hardware 
INSERT INTO hardware VALUES (1, 'hardware 1');
-- device
INSERT INTO device VALUES (1, 1, 'xxxxxxx', 1, 1, '2014-06-02', '2014-06-02', '2014-06-02', '2014-06-02', '2014-06-02');